<?xml version="1.0" encoding="UTF-8"?>
<!--
  Copyright 2014 Global Crop Diversity Trust
  
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  
    http://www.apache.org/licenses/LICENSE-2.0
  
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
-->

<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>

	<groupId>org.genesys-pgr</groupId>
	<artifactId>genesys2-server</artifactId>

	<name>Genesys 2 Server</name>
	<packaging>war</packaging>
	<version>0.9.1</version>
	<url>https://www.genesys-pgr.org</url>
	<description>Genesys 2 Server</description>

	<licenses>
		<license>
			<name>The Apache License, Version 2.0</name>
			<url>http://www.apache.org/licenses/LICENSE-2.0.txt</url>
		</license>
	</licenses>

	<scm>
		<connection>https://bitbucket.org/genesys2/genesys2-server.git</connection>
		<developerConnection>scm:git:git@bitbucket.org:genesys2/genesys2-server.git</developerConnection>
		<url>git@bitbucket.org:genesys2/genesys2-server.git</url>
	</scm>

	<organization>
		<name>Global Crop Diversity Trust</name>
		<url>https://www.croptrust.org</url>
	</organization>

	<issueManagement>
		<url>https://bitbucket.org/genesys2/genesys2-server/issues</url>
	</issueManagement>

	<properties>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<jdk.target>1.8</jdk.target>
		<jdk.source>1.8</jdk.source>
		<show.deprecations>false</show.deprecations>

		<junit.version>4.11</junit.version>
		<jsonpath.version>0.8.1</jsonpath.version>

		<commons.beanutils.version>1.8.3</commons.beanutils.version>
		<commons.collections.version>3.2.1</commons.collections.version>
		<commons.fileupload.version>1.2.2</commons.fileupload.version>
		<commons.io.version>2.4</commons.io.version>
		<commons.lang.version>2.6</commons.lang.version>
		<commons.logging.version>1.1.1</commons.logging.version>

		<jstl.version>1.2</jstl.version>
		<servlet-api.version>2.5</servlet-api.version>
		<jsp-api.version>2.1</jsp-api.version>

		<hibernate.version>4.3.6.Final</hibernate.version>
		<hsqldb.version>2.3.2</hsqldb.version>
		<ehcache.version>2.7.4</ehcache.version>

		<slf4j.version>1.7.7</slf4j.version>
		<log4j.version>1.2.17</log4j.version>

		<aspectj.version>1.7.2</aspectj.version>
		<spring.framework.version>4.2.1.RELEASE</spring.framework.version>
		<spring.security.version>4.0.2.RELEASE</spring.security.version>
		<spring.security.oauth2.version>1.0.5.RELEASE</spring.security.oauth2.version>
		<org.springframework.social-version>1.1.2.RELEASE</org.springframework.social-version>
		<org.springframework.social-google-version>1.0.0.RELEASE</org.springframework.social-google-version>

		<mysql.version>5.1.31</mysql.version>
		<hazelcast.version>3.4</hazelcast.version>

		<oval.version>1.81</oval.version>
		<jackson.version>2.2.1</jackson.version>
		<jaxb-api.version>2.1</jaxb-api.version>

		<!--Container -->
		<jetty.version>9.2.9.v20150224</jetty.version>

		<maven.test.skip>false</maven.test.skip>

		<war.name>genesys2</war.name>
	</properties>

	<dependencies>
		<!--Test dependencies -->
		<dependency>
			<groupId>junit</groupId>
			<artifactId>junit</artifactId>
			<version>${junit.version}</version>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>org.hamcrest</groupId>
			<artifactId>hamcrest-library</artifactId>
			<version>1.3</version>
			<scope>test</scope>
		</dependency>

		<dependency>
			<groupId>commons-beanutils</groupId>
			<artifactId>commons-beanutils</artifactId>
			<version>${commons.beanutils.version}</version>
		</dependency>


		<dependency>
			<groupId>commons-fileupload</groupId>
			<artifactId>commons-fileupload</artifactId>
			<version>${commons.fileupload.version}</version>
		</dependency>

		<dependency>
			<groupId>commons-io</groupId>
			<artifactId>commons-io</artifactId>
			<version>${commons.io.version}</version>
		</dependency>

		<dependency>
			<groupId>commons-lang</groupId>
			<artifactId>commons-lang</artifactId>
			<version>${commons.lang.version}</version>
		</dependency>

		<dependency>
			<groupId>commons-logging</groupId>
			<artifactId>commons-logging</artifactId>
			<version>${commons.logging.version}</version>
		</dependency>

		<dependency>
			<groupId>commons-codec</groupId>
			<artifactId>commons-codec</artifactId>
			<version>1.10</version>
		</dependency>

		<!-- Logging dependencies -->
		<dependency>
			<groupId>org.slf4j</groupId>
			<artifactId>slf4j-api</artifactId>
			<version>${slf4j.version}</version>
		</dependency>

		<dependency>
			<groupId>org.slf4j</groupId>
			<artifactId>slf4j-log4j12</artifactId>
			<version>${slf4j.version}</version>
		</dependency>

		<dependency>
			<groupId>log4j</groupId>
			<artifactId>log4j</artifactId>
			<version>${log4j.version}</version>
		</dependency>

		<dependency>
			<groupId>org.slf4j</groupId>
			<artifactId>jcl-over-slf4j</artifactId>
			<version>${slf4j.version}</version>
		</dependency>

		<!--Servlet and portal dependencies -->
		<dependency>
			<groupId>javax.servlet.jsp</groupId>
			<artifactId>javax.servlet.jsp-api</artifactId>
			<version>2.3.1</version>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>javax.servlet</groupId>
			<artifactId>javax.servlet-api</artifactId>
			<version>3.1.0</version>
			<scope>provided</scope>
		</dependency>

		<dependency>
			<groupId>javax.servlet</groupId>
			<artifactId>jstl</artifactId>
			<version>${jstl.version}</version>
			<scope>provided</scope>
		</dependency>

		<dependency>
			<groupId>javax.annotation</groupId>
			<artifactId>jsr250-api</artifactId>
			<version>1.0</version>
			<scope>provided</scope>
		</dependency>

		<!-- Spring dependencies -->
		<dependency>
			<groupId>org.aspectj</groupId>
			<artifactId>aspectjweaver</artifactId>
			<version>${aspectj.version}</version>
		</dependency>

		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-aop</artifactId>
			<version>${spring.framework.version}</version>
		</dependency>

		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-aspects</artifactId>
			<version>${spring.framework.version}</version>
		</dependency>

		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-context-support</artifactId>
			<version>${spring.framework.version}</version>
		</dependency>

		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-orm</artifactId>
			<version>${spring.framework.version}</version>
		</dependency>

		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-tx</artifactId>
			<version>${spring.framework.version}</version>
		</dependency>

		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-webmvc</artifactId>
			<version>${spring.framework.version}</version>
		</dependency>

		<dependency>
			<groupId>org.springframework.security</groupId>
			<artifactId>spring-security-config</artifactId>
			<version>${spring.security.version}</version>
		</dependency>

		<dependency>
			<groupId>org.springframework.security</groupId>
			<artifactId>spring-security-taglibs</artifactId>
			<version>${spring.security.version}</version>
		</dependency>


		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-test</artifactId>
			<version>${spring.framework.version}</version>
			<scope>test</scope>
		</dependency>

		<dependency>
			<groupId>org.springframework.data</groupId>
			<artifactId>spring-data-jpa</artifactId>
			<version>1.9.0.RELEASE</version>
		</dependency>

		<!-- Hibernate dependencies -->


		<dependency>
			<groupId>org.hibernate</groupId>
			<artifactId>hibernate-entitymanager</artifactId>
			<version>${hibernate.version}</version>
		</dependency>

		<dependency>
			<groupId>org.hsqldb</groupId>
			<artifactId>hsqldb</artifactId>
			<version>${hsqldb.version}</version>
		</dependency>

		<dependency>
			<groupId>mysql</groupId>
			<artifactId>mysql-connector-java</artifactId>
			<version>${mysql.version}</version>
		</dependency>

		<!--Other dependencies -->

		<dependency>
			<groupId>net.sf.oval</groupId>
			<artifactId>oval</artifactId>
			<version>${oval.version}</version>
		</dependency>

		<dependency>
			<groupId>com.fasterxml.jackson.core</groupId>
			<artifactId>jackson-databind</artifactId>
			<version>${jackson.version}</version>
		</dependency>
		<dependency>
			<groupId>javax.xml.bind</groupId>
			<artifactId>jaxb-api</artifactId>
			<version>${jaxb-api.version}</version>
			<scope>runtime</scope>
		</dependency>

		<!--Jetty -->


		<dependency>
			<groupId>net.tanesha.recaptcha4j</groupId>
			<artifactId>recaptcha4j</artifactId>
			<version>0.0.7</version>
		</dependency>


		<dependency>
			<groupId>net.sf.opencsv</groupId>
			<artifactId>opencsv</artifactId>
			<version>2.3</version>
		</dependency>
		<dependency>
			<groupId>org.sitemesh</groupId>
			<artifactId>sitemesh</artifactId>
			<version>3.0-alpha-2</version>
		</dependency>
		<dependency>
			<groupId>org.apache.tomcat</groupId>
			<artifactId>tomcat-jdbc</artifactId>
			<version>7.0.42</version>
		</dependency>
		<dependency>
			<groupId>com.googlecode.owasp-java-html-sanitizer</groupId>
			<artifactId>owasp-java-html-sanitizer</artifactId>
			<version>r209</version>
		</dependency>
		<dependency>
			<groupId>org.springframework.security.oauth</groupId>
			<artifactId>spring-security-oauth2</artifactId>
			<version>${spring.security.oauth2.version}</version>
		</dependency>
		<dependency>
			<groupId>joda-time</groupId>
			<artifactId>joda-time</artifactId>
			<version>2.3</version>
		</dependency>
		<dependency>
			<groupId>org.apache.commons</groupId>
			<artifactId>commons-collections4</artifactId>
			<version>4.0</version>
		</dependency>
		<dependency>
			<groupId>javax.mail</groupId>
			<artifactId>mail</artifactId>
			<version>1.5.0-b01</version>
		</dependency>
		<dependency>
			<groupId>org.apache.velocity</groupId>
			<artifactId>velocity</artifactId>
			<version>1.7</version>
		</dependency>
		<dependency>
			<groupId>com.hazelcast</groupId>
			<artifactId>hazelcast-spring</artifactId>
			<version>${hazelcast.version}</version>
		</dependency>
		<dependency>
			<groupId>com.hazelcast</groupId>
			<artifactId>hazelcast-cloud</artifactId>
			<version>${hazelcast.version}</version>
		</dependency>
		<dependency>
			<groupId>com.hazelcast</groupId>
			<artifactId>hazelcast-hibernate4</artifactId>
			<version>${hazelcast.version}</version>
		</dependency>
		<dependency>
			<groupId>com.hazelcast</groupId>
			<artifactId>hazelcast-wm</artifactId>
			<version>${hazelcast.version}</version>
		</dependency>



		<dependency>
			<groupId>org.jamel.dbf</groupId>
			<artifactId>dbf-reader</artifactId>
			<version>0.1.0</version>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>org.eclipse.jetty</groupId>
			<artifactId>jetty-servlets</artifactId>
			<version>${jetty.version}</version>
			<scope>runtime</scope>
		</dependency>


		<dependency>
			<groupId>org.elasticsearch</groupId>
			<artifactId>elasticsearch</artifactId>
			<version>1.3.2</version>
		</dependency>

		<dependency>
			<groupId>org.springframework.data</groupId>
			<artifactId>spring-data-elasticsearch</artifactId>
			<version>1.3.0.RELEASE</version>
		</dependency>



		<dependency>
			<groupId>org.springframework.social</groupId>
			<artifactId>spring-social-google</artifactId>
			<version>${org.springframework.social-google-version}</version>
		</dependency>

		<dependency>
			<groupId>org.springframework.social</groupId>
			<artifactId>spring-social-web</artifactId>
			<version>${org.springframework.social-version}</version>
		</dependency>

		<dependency>
			<groupId>org.springframework.social</groupId>
			<artifactId>spring-social-core</artifactId>
			<version>${org.springframework.social-version}</version>
		</dependency>

		<dependency>
			<groupId>xml-apis</groupId>
			<artifactId>xml-apis</artifactId>
			<version>1.4.01</version>
		</dependency>
		<dependency>
			<groupId>org.tuckey</groupId>
			<artifactId>urlrewritefilter</artifactId>
			<version>4.0.4</version>
		</dependency>

		<dependency>
			<groupId>org.apache.poi</groupId>
			<artifactId>poi-ooxml</artifactId>
			<version>3.10.1</version>
		</dependency>
		<dependency>
			<groupId>org.ocpsoft.prettytime</groupId>
			<artifactId>prettytime</artifactId>
			<version>3.2.5.Final</version>
		</dependency>
		<dependency>
			<groupId>org.apache.httpcomponents</groupId>
			<artifactId>httpclient</artifactId>
			<version>4.3.6</version>
		</dependency>
		<dependency>
			<groupId>org.genesys-pgr</groupId>
			<artifactId>transifex-client</artifactId>
			<version>0.5-SNAPSHOT</version>
		</dependency>
		<dependency>
			<groupId>org.genesys-pgr</groupId>
			<artifactId>worldclim-reader</artifactId>
			<version>0.0.3-SNAPSHOT</version>
		</dependency>
		<dependency>
			<groupId>org.genesys-pgr</groupId>
			<artifactId>pgr-ontology-model</artifactId>
			<version>0.7.2-SNAPSHOT</version>
		</dependency>
        <dependency>
            <groupId>org.mockito</groupId>
            <artifactId>mockito-all</artifactId>
            <version>2.0.2-beta</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>com.jayway.jsonpath</groupId>
            <artifactId>json-path-assert</artifactId>
            <version>0.8.1</version>
            <scope>test</scope>
        </dependency>
        <dependency>
        	<groupId>org.jsoup</groupId>
        	<artifactId>jsoup</artifactId>
        	<version>1.8.2</version>
        </dependency>
	</dependencies>

	<build>
		<plugins>
			<plugin>
				<inherited>true</inherited>
				<artifactId>maven-resources-plugin</artifactId>
				<version>2.6</version>
				<configuration>
					<encoding>UTF-8</encoding>
					<detail>true</detail>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.codehaus.mojo</groupId>
				<artifactId>buildnumber-maven-plugin</artifactId>
				<version>1.3</version>
				<executions>
					<execution>
						<phase>validate</phase>
						<goals>
							<goal>create</goal>
						</goals>
					</execution>
				</executions>
				<configuration>
					<!-- first check to see if you have locally modified files, and will 
						fail if there are any. -->
					<doCheck>false</doCheck>
					<doUpdate>false</doUpdate>
				</configuration>
			</plugin>
			<plugin>
				<inherited>true</inherited>
				<artifactId>maven-compiler-plugin</artifactId>
				<version>3.1</version>
				<configuration>
					<source>${jdk.source}</source>
					<target>${jdk.target}</target>
					<optimize>true</optimize>
					<showDeprecation>${show.deprecations}</showDeprecation>
					<showWarnings>true</showWarnings>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-surefire-plugin</artifactId>
				<version>2.17</version>
				<configuration>
					<forkMode>once</forkMode>
					<argLine>-Xms512m -Xmx1024m</argLine>
					<testFailureIgnore>false</testFailureIgnore>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.eclipse.jetty</groupId>
				<artifactId>jetty-maven-plugin</artifactId>
				<version>${jetty.version}</version>
				<configuration>
					<stopPort>8888</stopPort>
					<stopKey>stop</stopKey>
					<jvmArgs>-Dspring.profiles.active=dev -Xmx2048M -Xms1024M -XX:MaxPermSize=128M -Djava.awt.headless=true -server</jvmArgs>
				</configuration>
			</plugin>
			<plugin>
				<groupId>com.github.eirslett</groupId>
				<artifactId>frontend-maven-plugin</artifactId>
				<version>0.0.23</version>
				<executions>
					<execution>
						<id>install node and npm</id>
						<goals>
							<goal>install-node-and-npm</goal>
						</goals>
						<configuration>
							<nodeVersion>v0.10.36</nodeVersion>
							<npmVersion>2.4.1</npmVersion>
						</configuration>
					</execution>
					<execution>
						<id>npm install</id>
						<goals>
							<goal>npm</goal>
						</goals>
						<configuration>
							<arguments>install</arguments>
						</configuration>
					</execution>
					<execution>
						<id>bower install</id>
						<goals>
							<goal>bower</goal>
						</goals>
						<configuration>
							<arguments>install</arguments>
						</configuration>
					</execution>
					<execution>
						<id>grunt build</id>
						<goals>
							<goal>grunt</goal>
						</goals>
						<configuration>
							<arguments>build</arguments>
							<srcdir>${basedir}/src/main/sourceapp</srcdir>
							<outputdir>${basedir}/src/main/webapp/html</outputdir>
							<triggerfiles>
								<triggerfile>Gruntfile.js</triggerfile>
								<triggerfile>package.json</triggerfile>
							</triggerfiles>
						</configuration>
					</execution>
				</executions>
			</plugin>
			<plugin>
				<artifactId>maven-assembly-plugin</artifactId>
				<version>2.4</version>
				<configuration>
					<descriptors>
						<descriptor>assembly/assembly-jetty.xml</descriptor>
					</descriptors>
				</configuration>
				<executions>
					<execution>
						<id>make-assembly</id> <!-- this is used for inheritance merges -->
						<phase>package</phase> <!-- bind to the packaging phase -->
						<goals>
							<goal>single</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-release-plugin</artifactId>
				<version>2.5.1</version>
				<configuration>
					<pushChanges>false</pushChanges>
				</configuration>
			</plugin>
		</plugins>

		<resources>
			<resource>
				<directory>src/main/resources</directory>
				<filtering>false</filtering>
			</resource>
			<resource>
				<directory>src/main/resources</directory>
				<includes>
					<include>application.properties</include>
				</includes>
				<filtering>true</filtering>
			</resource>
		</resources>
		<pluginManagement>
			<plugins>
				<!--This plugin's configuration is used to store Eclipse m2e settings 
					only. It has no influence on the Maven build itself. -->
				<plugin>
					<groupId>org.eclipse.m2e</groupId>
					<artifactId>lifecycle-mapping</artifactId>
					<version>1.0.0</version>
					<configuration>
						<lifecycleMappingMetadata>
							<pluginExecutions>
								<pluginExecution>
									<pluginExecutionFilter>
										<groupId>
											net.alchim31.maven
										</groupId>
										<artifactId>
											yuicompressor-maven-plugin
										</artifactId>
										<versionRange>
											[1.4.0,)
										</versionRange>
										<goals>
											<goal>compress</goal>
										</goals>
									</pluginExecutionFilter>
									<action>
										<ignore></ignore>
									</action>
								</pluginExecution>
							</pluginExecutions>
						</lifecycleMappingMetadata>
					</configuration>
				</plugin>
			</plugins>
		</pluginManagement>
	</build>
</project>

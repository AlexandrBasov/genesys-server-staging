package org.genesys2.tests.resttests;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.genesys2.server.exception.UserException;
import org.genesys2.server.model.UserRole;
import org.genesys2.server.model.genesys.Method;
import org.genesys2.server.model.impl.User;
import org.genesys2.server.model.oauth.OAuthClientDetails;
import org.genesys2.server.model.oauth.OAuthClientType;
import org.genesys2.server.model.oauth.OAuthRefreshToken;
import org.genesys2.server.security.AuthUserDetails;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.DefaultAuthorizationRequest;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import java.nio.charset.Charset;
import java.util.*;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class OAuthManagementControllerTest extends AbstractRestTest {

    private static final Log LOG = LogFactory.getLog(OAuthManagementControllerTest.class);

    @Autowired
    private WebApplicationContext webApplicationContext;

    MockMvc mockMvc;

    private String JSON_OK = "{\"result\":true}";
    private MediaType APPLICATION_JSON_UTF8 = new MediaType(MediaType.APPLICATION_JSON.getType(), MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));
    private User user;
    private OAuth2AccessToken accessToken;
    private OAuthRefreshToken refreshToken;
    private OAuthClientDetails oAuthClientDetails;

    @Before
    public void setup() throws UserException {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();

        oAuthClientDetails = clientDetailsService.addClientDetails("title", "description", "redirectUri", 1, 2, OAuthClientType.SERVICE);

        user = new User();
        user.setEmail("salexandrbasov@gmail.com");
        user.setPassword("Alexandr19011990");
        user.setName("SYS_ADMIN");
        user.setUuid("SYS_ADMIN");

        userService.addUser(user);

        Map<Integer, Boolean> permission = new HashMap<>();
        permission.put(4, true);
        permission.put(1, false);
        permission.put(2, false);
        permission.put(8, false);
        permission.put(16, true);

        aclService.addPermissions(user.getId(), Method.class.getName(), "SYS_ADMIN", true, permission);

        HashMap<String, String> authorizationParameters = new HashMap<String, String>();
        authorizationParameters.put("scope", "read");
        authorizationParameters.put("username", user.getName());
        authorizationParameters.put("client_id", user.getName());
        authorizationParameters.put("grant", user.getPassword());

        DefaultAuthorizationRequest authorizationRequest = new DefaultAuthorizationRequest(authorizationParameters);
        authorizationRequest.setApproved(true);

        List<GrantedAuthority> authorities = new ArrayList<>();
        GrantedAuthority simpleGrantedAuthority = new SimpleGrantedAuthority(UserRole.ADMINISTRATOR.getName());
        authorities.add(simpleGrantedAuthority);

        authorizationRequest.setAuthorities(authorities);

        HashSet<String> resourceIds = new HashSet<String>();
        resourceIds.add(user.getName());
        authorizationRequest.setResourceIds(resourceIds);
        oAuthClientDetails.setResourceIds(user.getUuid());
        AuthUserDetails authUserDetails = new AuthUserDetails(user.getUuid(), user.getPassword(), authorities);

        // set actual DB user
        authUserDetails.setUser(user);

        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(authUserDetails, authUserDetails, authorities);

        OAuth2Authentication authenticationRequest = new OAuth2Authentication(authorizationRequest, authenticationToken);
        authenticationRequest.setAuthenticated(true);

        accessToken = ((DefaultTokenServices) tokenServices).createAccessToken(authenticationRequest);
        refreshToken = new OAuthRefreshToken();
        refreshToken.setValue("Value");
        refreshToken.setClientId(user.getName());
        refreshTokenPersistence.save(refreshToken);

        SecurityContextHolder.getContext().setAuthentication(authenticationRequest);
    }

    @After
    public void teerDown() {
        clientDetailsPersistence.deleteAll();
        accessTokenPersistence.deleteAll();
        refreshTokenPersistence.deleteAll();
        userPersistence.deleteAll();
    }

    @Test
    public void listClientsTest() throws Exception {
        LOG.info("Start test-method listClientsTestTest");

        ObjectMapper objectMapper = new ObjectMapper();

        mockMvc.perform(get("/api/v0/oauth/clientslist")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON_UTF8))
                .andExpect(content().string(objectMapper.writeValueAsString(clientDetailsService.listClientDetails())));

        LOG.info("Test listClientsTest passed");
    }

    @Test
    public void removeAccessTokenTest() throws Exception {
        LOG.info("Start test-method removeAccessTokenTest");

        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode ok = objectMapper.readTree(JSON_OK);

        mockMvc.perform(delete("/api/v0/oauth/token/at/{tokenId}/remove", accessTokenPersistence.findAll().get(0).getId())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(new MediaType(MediaType.TEXT_PLAIN.getType(), MediaType.TEXT_PLAIN.getSubtype(), Charset.forName("ISO-8859-1"))))
                .andExpect(content().string(objectMapper.writeValueAsString(ok)));

        LOG.info("Test removeAccessTokenTest passed");
    }

    @Test
    public void removeRefreshTokenTest() throws Exception {
        LOG.info("Start test-method removeRefreshTokenTest");

        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode ok = objectMapper.readTree(JSON_OK);

        mockMvc.perform(delete("/api/v0/oauth/token/rt/{tokenId}/remove", refreshTokenPersistence.findAll().get(0).getId())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(new MediaType(MediaType.TEXT_PLAIN.getType(), MediaType.TEXT_PLAIN.getSubtype(), Charset.forName("ISO-8859-1"))))
                .andExpect(content().string(objectMapper.writeValueAsString(ok)));

        LOG.info("Test removeRefreshTokenTest passed");
    }

    @Test
    public void removeAllAccessTokensTest() throws Exception {
        LOG.info("Start test-method removeAllAccessTokensTest");

        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode ok = objectMapper.readTree(JSON_OK);

        mockMvc.perform(delete("/api/v0/oauth/token/{clientId}/remove-all-at", user.getUuid())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(new MediaType(MediaType.TEXT_PLAIN.getType(), MediaType.TEXT_PLAIN.getSubtype(), Charset.forName("ISO-8859-1"))))
                .andExpect(content().string(objectMapper.writeValueAsString(ok)));

        LOG.info("Test removeAllAccessTokensTest passed");
    }

    @Test
    public void removeAllRefreshTokensTest() throws Exception {
        LOG.info("Start test-method removeAllRefreshTokensTest");

        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode ok = objectMapper.readTree(JSON_OK);

        mockMvc.perform(delete("/api/v0/oauth/token/{clientId}/remove-all-rt", user.getUuid())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(new MediaType(MediaType.TEXT_PLAIN.getType(), MediaType.TEXT_PLAIN.getSubtype(), Charset.forName("ISO-8859-1"))))
                .andExpect(content().string(objectMapper.writeValueAsString(ok)));

        LOG.info("Test removeAllRefreshTokensTest passed");
    }

    @Test
    public void saveClientEntryTest() throws Exception {
        LOG.info("Start test-method saveClientEntryTest");

        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode ok = objectMapper.readTree("{\"clientId\":\"" + oAuthClientDetails.getClientId() + "\"}");

        mockMvc.perform(post("/api/v0/oauth/save-client")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(oAuthClientDetails)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(new MediaType(MediaType.TEXT_PLAIN.getType(), MediaType.TEXT_PLAIN.getSubtype(), Charset.forName("ISO-8859-1"))))
                .andExpect(content().string(objectMapper.writeValueAsString(ok)));

        LOG.info("Test saveClientEntryTest passed");
    }

    @Test
    public void deleteClientTest() throws Exception {
        LOG.info("Start test-method deleteClientTest");

        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode ok = objectMapper.readTree(JSON_OK);

        mockMvc.perform(post("/api/v0/oauth/delete-client")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(oAuthClientDetails)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(new MediaType(MediaType.TEXT_PLAIN.getType(), MediaType.TEXT_PLAIN.getSubtype(), Charset.forName("ISO-8859-1"))))
                .andExpect(content().string(objectMapper.writeValueAsString(ok)));

        LOG.info("Test deleteClientTest passed");
    }

    @Test
    public void clientDetailsTest() throws Exception {
        LOG.info("Start test-method clientDetailsTest");

        ObjectMapper objectMapper = new ObjectMapper();

        mockMvc.perform(get("/api/v0/oauth/{id}", oAuthClientDetails.getId())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON_UTF8))
                .andExpect(content().string(objectMapper.writeValueAsString(oAuthClientDetails)));

        LOG.info("Test clientDetailsTest passed");
    }

    @Test
    public void clientTokensTest() throws Exception {
        LOG.info("Start test-method clientTokensTest");

        mockMvc.perform(get("/api/v0/oauth/tokens/{clientId}", user.getName())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", is(HashMap.class)))
                .andExpect(jsonPath("$.accessTokens", hasSize(1)))
                .andExpect(jsonPath("$.refreshTokens", hasSize(1)));

        LOG.info("Test clientTokensTest passed");
    }

    @Test
    @Transactional
    public void getPermissionsTest() throws Exception {
        LOG.info("Start test-method getPermissionsTest");

        mockMvc.perform(get("/api/v0/oauth/permissions/{id}", user.getId())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", is(HashMap.class)))
                .andExpect(jsonPath("$.users", hasSize(1)));

        LOG.info("Test getPermissionsTest passed");
    }

    @Test
    public void getWidgetTest() throws Exception {
        LOG.info("Start test-method getWidgetTest");

        mockMvc.perform(get("/api/v0/oauth/client/{clientId}/get_widget", oAuthClientDetails.getClientId())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", is(HashMap.class)))
                .andExpect(jsonPath("$.clientDetails", hasSize(1)))
                .andExpect(jsonPath("$.clientDetails[0].clientId", is(oAuthClientDetails.getClientId())));

        LOG.info("Test getWidgetTest passed");
    }
}

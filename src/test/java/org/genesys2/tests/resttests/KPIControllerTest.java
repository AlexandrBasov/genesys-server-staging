package org.genesys2.tests.resttests;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.genesys2.server.model.kpi.*;
import org.genesys2.server.servlet.controller.rest.model.ExecutionJson;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.*;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class KPIControllerTest extends AbstractRestTest {

    private static final Log LOG = LogFactory.getLog(KPIControllerTest.class);

    @Autowired
    private WebApplicationContext webApplicationContext;

    MockMvc mockMvc;

    private KPIParameter kpiParameter;
    private Dimension dimension;
    private Execution execution;
    private Observation observation;
    private ExecutionRun executionRun;
    private DimensionKey dimensionKey;

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();

        dimension = new BooleanDimension();
        dimension.setVersion(1);
        dimension.setTitle("DimensionTitle");
        dimension.setName("DimensionName");

        dimensionKey = new DimensionKey();
        dimensionKey.setName("NameDimKey");
        dimensionKey.setValue("ValueDimKey");

        kpiParameter = new KPIParameter();
        kpiParameter.setName("KPIParamName");
        kpiParameter.setTitle("KPIParamTitle");
        kpiParameter.setEntity(KPIParameter.class.getName());
        kpiParameter.setVersion(1);
        kpiParameter.setDescription("DescriptionKPI");

        kpiService.save(kpiParameter);
        kpiService.save(dimension);

        execution = new Execution();
        execution.setName("ExecutionName");
        execution.setVersion(1);
        execution.setParameter(kpiParameter);

        executionRun = new ExecutionRun();

        executionRun.setExecution(execution);
        executionRun.setTimestamp(new Date());

        observation = new Observation();
        observation.setExecutionRun(executionRun);
        observation.setValue(1);

        Set<DimensionKey> dimensionKeys = new HashSet<>();
        dimensionKeys.add(dimensionKey);

        observation.setDimensions(dimensionKeys);

        kpiService.save(execution);
        List<Observation> observationList = new ArrayList<>();
        observationList.add(observation);
        kpiService.save(execution, observationList);
    }

    @After
    public void tearDown() {
        observationRepository.deleteAll();
        for (int i = kpiService.listExecutions().size() - 1; i >= 0; i--) {
            kpiService.delete(kpiService.listExecutions().get(i));
        }
        kpiParameterRepository.deleteAll();
    }

    @Test
    public void listParametersTest() throws Exception {
        LOG.info("Start test-method listParametersTest");

        mockMvc.perform(get("/api/v0/kpi/parameter/list")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.KPIParamName", is("KPIParamTitle\nDescriptionKPI")));

        LOG.info("Test listParametersTest passed");
    }

    @Test
    public void getParameterTest() throws Exception {
        LOG.info("Start test-method getParameterTest");

        ObjectMapper objectMapper = new ObjectMapper();

        mockMvc.perform(get("/api/v0/kpi/parameter/{name}", "KPIParamName")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string(objectMapper.writeValueAsString(kpiParameter)));

        LOG.info("Test getParameterTest passed");
    }

    @Test
    public void deleteParameterTest() throws Exception {
        LOG.info("Start test-method deleteParameterTest");

        observationRepository.deleteAll();
        for (int i = kpiService.listExecutions().size() - 1; i >= 0; i--) {
            kpiService.delete(kpiService.listExecutions().get(i));
        }

        mockMvc.perform(delete("/api/v0/kpi/parameter/{name}", "KPIParamName")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id", is(nullValue())))
                .andExpect(jsonPath("$.name", is("KPIParamName")))
                .andExpect(jsonPath("$.title", is("KPIParamTitle")))
                .andExpect(jsonPath("$.entity", is(KPIParameter.class.getName())))
                .andExpect(jsonPath("$.description", is("DescriptionKPI")));

        LOG.info("Test deleteParameterTest passed");
    }

    @Test
    public void createParameterTest() throws Exception {
        LOG.info("Start test-method createParameterTest");

        tearDown();

        ObjectMapper objectMapper = new ObjectMapper();

        mockMvc.perform(post("/api/v0/kpi/parameter")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(kpiParameter)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id", is(notNullValue())))
                .andExpect(jsonPath("$.name", is("KPIParamName")))
                .andExpect(jsonPath("$.title", is("KPIParamTitle")))
                .andExpect(jsonPath("$.entity", is(KPIParameter.class.getName())))
                .andExpect(jsonPath("$.description", is("DescriptionKPI")));

        LOG.info("Test createParameterTest passed");
    }

    @Test
    public void listDimensionsTest() throws Exception {
        LOG.info("Start test-method listDimensionsTest");

        mockMvc.perform(get("/api/v0/kpi/dimension/list")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$." + dimension.getId(), is(dimension.getName() + " " + dimension.getTitle())));

        LOG.info("Test listDimensionsTest passed");
    }

    @Test
    public void getDimensionTest() throws Exception {
        LOG.info("Start test-method getDimensionTest");

        ObjectMapper objectMapper = new ObjectMapper();

        mockMvc.perform(get("/api/v0/kpi/dimension/{id}", dimension.getId())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string(objectMapper.writeValueAsString(dimension)));

        LOG.info("Test getDimensionTest passed");
    }

    @Test
    public void deleteDimensionTest() throws Exception {
        LOG.info("Start test-method deleteDimensionTest");

        mockMvc.perform(delete("/api/v0/kpi/dimension/{id}", dimension.getId())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id", is(nullValue())))
                .andExpect(jsonPath("$.name", is("DimensionName")))
                .andExpect(jsonPath("$.title", is("DimensionTitle")));

        LOG.info("Test deleteDimensionTest passed");
    }

    @Test
    public void updateBooleanDimensionTest() throws Exception {
        LOG.info("Start test-method updateBooleanDimensionTest");

        tearDown();

        ObjectMapper objectMapper = new ObjectMapper();

        mockMvc.perform(post("/api/v0/kpi/dimension")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(dimension)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id", is(dimension.getId().intValue())))
                .andExpect(jsonPath("$.name", is("DimensionName")))
                .andExpect(jsonPath("$.title", is("DimensionTitle")));

        LOG.info("Test updateBooleanDimensionTest passed");
    }

    @Test
    public void listExecutionTest() throws Exception {
        LOG.info("Start test-method listExecutionTest");

        mockMvc.perform(get("/api/v0/kpi/execution/list")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$." + execution.getName(), is(execution.getTitle())));

        LOG.info("Test listExecutionTest passed");
    }

    @Test
    public void getExecutionTest() throws Exception {
        LOG.info("Start test-method getExecutionTest");

        mockMvc.perform(get("/api/v0/kpi/execution/{executionName}", execution.getName())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id", is(execution.getId().intValue())))
                .andExpect(jsonPath("$.name", is(execution.getName())))
                .andExpect(jsonPath("$.version", is((int) execution.getVersion())))
                .andExpect(jsonPath("$.parameter", is(execution.getParameter().getName())))
                .andExpect(jsonPath("$.title", isEmptyOrNullString()))
                .andExpect(jsonPath("$.dimensions", hasSize(0)));

        LOG.info("Test getExecutionTest passed");
    }

    @Test
    public void deleteExecutionTest() throws Exception {
        LOG.info("Start test-method deleteExecutionTest");
        observationRepository.deleteAll();
        mockMvc.perform(delete("/api/v0/kpi/execution/{executionName}", execution.getName())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id", is(nullValue())))
                .andExpect(jsonPath("$.name", is(execution.getName())))
                .andExpect(jsonPath("$.version", is((int) execution.getVersion())))
                .andExpect(jsonPath("$.parameter", is(execution.getParameter().getName())))
                .andExpect(jsonPath("$.title", is(execution.getTitle())));

        LOG.info("Test deleteExecutionTest passed");
    }

    @Test
    public void listObservationsTest() throws Exception {
        LOG.info("Start test-method listObservationsTest");

        mockMvc.perform(post("/api/v0/kpi/observation/{executionName}/", execution.getName())
                .contentType(MediaType.APPLICATION_JSON)
                .param("page", "1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id", is(notNullValue())))
                .andExpect(jsonPath("$[0].value", is(1.0)))
                .andExpect(jsonPath("$[0].dimensions", hasSize(1)))
                .andExpect(jsonPath("$[0].dimensionCount", is(1)));

        LOG.info("Test listObservationsTest passed");
    }

    @Test
    public void executeTest() throws Exception {
        LOG.info("Start test-method executeTest");

        mockMvc.perform(post("/api/v0/kpi/execution/{executionName}/execute", execution.getName())
                .contentType(MediaType.APPLICATION_JSON)
                .param("page", "1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id", is(notNullValue())))
                .andExpect(jsonPath("$[0].value", is(1.0)))
                .andExpect(jsonPath("$[0].dimensions", hasSize(0)))
                .andExpect(jsonPath("$[0].dimensionCount", is(0)));

        LOG.info("Test executeTest passed");
    }

    @Test
    public void updateExecutionTest() throws Exception {
        LOG.info("Start test-method updateExecutionTest");

        ObjectMapper objectMapper = new ObjectMapper();

        ExecutionJson executionJson = ExecutionJson.from(execution);

        mockMvc.perform(post("/api/v0/kpi/execution")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(executionJson)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id", is(notNullValue())))
                .andExpect(jsonPath("$.name", is("ExecutionName")))
                .andExpect(jsonPath("$.version", is(1)))
                .andExpect(jsonPath("$.parameter", is("KPIParamName")))
                .andExpect(jsonPath("$.title", is(nullValue())))
                .andExpect(jsonPath("$.dimensions", hasSize(0)));

        LOG.info("Test updateExecutionTest passed");
    }
}

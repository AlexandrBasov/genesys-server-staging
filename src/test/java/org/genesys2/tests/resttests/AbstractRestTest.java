package org.genesys2.tests.resttests;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.velocity.app.VelocityEngine;
import org.genesys2.server.aspect.AsAdminAspect;
import org.genesys2.server.persistence.acl.AclClassPersistence;
import org.genesys2.server.persistence.acl.AclEntryPersistence;
import org.genesys2.server.persistence.acl.AclObjectIdentityPersistence;
import org.genesys2.server.persistence.acl.AclSidPersistence;
import org.genesys2.server.persistence.domain.*;
import org.genesys2.server.persistence.domain.kpi.KPIParameterRepository;
import org.genesys2.server.persistence.domain.kpi.ObservationRepository;
import org.genesys2.server.persistence.domain.mock.TraitServiceMock;
import org.genesys2.server.service.*;
import org.genesys2.server.service.impl.*;
import org.genesys2.server.servlet.controller.rest.*;
import org.genesys2.server.test.JpaDataConfig;
import org.genesys2.server.test.PropertyPlacholderInitializer;
import org.genesys2.spring.config.ElasticsearchConfig;
import org.genesys2.spring.config.HazelcastConfig;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.CacheManager;
import org.springframework.cache.support.NoOpCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.converter.json.Jackson2ObjectMapperFactoryBean;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.security.oauth2.provider.token.ConsumerTokenServices;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = AbstractRestTest.Config.class, initializers = PropertyPlacholderInitializer.class)
@ActiveProfiles("dev")
@WebAppConfiguration
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)
public abstract class AbstractRestTest {

    @Configuration
    @EnableWebMvc
    @Import({JpaDataConfig.class, HazelcastConfig.class, ElasticsearchConfig.class})
    @PropertySource({"classpath:application.properties", "classpath:spring/spring.properties"})
    public static class Config {

        @Bean
        public RequestsController requestsController() {
            return new RequestsController();
        }

        @Bean
        public RequestService requestService() {
            return new RequestServiceImpl();
        }

        @Bean
        public GeoService geoService() {
            return new GeoServiceImpl();
        }

        @Bean
        public UserController userController() {
            return new UserController();
        }

        @Bean
        public CacheManager cacheManager() {
            return new NoOpCacheManager();
        }

        @Bean
        public UserService userService() {
            return new UserServiceImpl();
        }

        @Bean
        public TeamService teamService() {
            return new TeamServiceImpl();
        }

        @Bean
        public InstituteService instituteService() {
            return new InstituteServiceImpl();
        }

        @Bean
        public ContentService contentService() {
            return new ContentServiceImpl();
        }

        @Bean
        public HtmlSanitizer htmlSanitizer() {
            return new OWASPSanitizer();
        }

        @Bean
        public VelocityEngine velocityEngine() {
            return new VelocityEngine();
        }

        @Bean
        @Qualifier("genesysLowlevelRepositoryCustomImpl")
        public GenesysLowlevelRepository genesysLowlevelRepositoryCustomImpl() {
            return new GenesysLowlevelRepositoryCustomImpl();
        }

        @Bean
        public AclService aclService() {
            return new AclServiceImpl();
        }

        @Bean
        public AsAdminAspect asAdminAspect() {
            return new AsAdminAspect();
        }

        @Bean
        public UsersController restUsersController() {
            return new UsersController();
        }

        @Bean
        public OAuth2ClientDetailsService auth2ClientDetailsService() {
            return new OAuth2ClientDetailsServiceImpl();
        }

        @Bean
        public EMailVerificationService emailVerificationService() {
            return new EMailVerificationServiceImpl();
        }

        @Bean
        public TokenVerificationService tokenVerificationService() {
            return new TokenVerificationServiceImpl();
        }

        @Bean
        public EMailService eMailService() {
            return new EMailServiceImpl();
        }

        @Bean
        public JavaMailSender mailSender() {
            return Mockito.mock(JavaMailSenderImpl.class);
        }

        @Bean
        public ThreadPoolTaskExecutor threadPoolTaskExecutor() {
            return new ThreadPoolTaskExecutor();
        }

        @Bean
        public TraitsController traitsController() {
            return new TraitsController();
        }

        @Bean
        public TraitService traitServiceMock() {
            return new TraitServiceMock();
        }

        @Bean
        public GenesysService genesysService() {
            return new GenesysServiceImpl();
        }

        @Bean
        public CropService cropService() {
            return new CropServiceImpl();
        }

        @Bean
        public TaxonomyService taxonomyService() {
            return new TaxonomyServiceImpl();
        }

        @Bean
        public TraitValueRepository traitValueRepository() {
            return new TraitValueRepositoryImpl();
        }

        @Bean
        public OrganizationService organizationService() {
            return new OrganizationServiceImpl();
        }

        @Bean
        public LookupController lookupController() {
            return new LookupController();
        }

        @Bean
        public PermissionController permissionController() {
            return new PermissionController();
        }

        @Bean
        public OrganizationController organizationController() {
            return new OrganizationController();
        }

        @Bean
        public BatchRESTService batchRESTService() {
            return new BatchRESTServiceImpl();
        }

        @Bean
        public AccessionCustomRepository accessionCustomRepository() {
            return new AccessionCustomRepositoryImpl();
        }

        @Bean
        public TaxonomyManager taxonomyManager() {
            return new TaxonomyManager();
        }

        @Bean
        public EasySMTA easySMTAConnector() {
            return new EasySMTAMockConnector();
        }

        @Bean
        public HttpClientBuilder httpClientBuilder() {
            return HttpClientBuilder.create();
        }

        @Bean
        public TokenController tokenController() {
            return new TokenController();
        }

        @Bean
        public ConsumerTokenServices consumerTokenServices() {
            DefaultTokenServices tokenServices = new DefaultTokenServices();
            tokenServices.setTokenStore(tokenStore());
            return tokenServices;
        }

        @Bean
        public TokenStore tokenStore() {
            return new OAuth2JPATokenStoreImpl();
        }

        @Bean
        public OAuthManagementController oAuthManagementController() {
            return new OAuthManagementController();
        }

        @Bean
        public KPIController kpiController() {
            return new KPIController();
        }

        @Bean
        public KPIService kpiService() {
            return new KPIServiceImpl();
        }

        @Bean
        public DatasetController datasetController() {
            return new DatasetController();
        }

        @Bean
        public DatasetService datasetService() {
            return new GenesysServiceImpl();
        }

        @Bean
        public CropsController cropsController() {
            return new CropsController();
        }

        @Bean
        public CacheController cacheController() {
            return new CacheController();
        }

        @Bean
        public MappingService mappingService() {
            return new MappingServiceImpl();
        }

        @Bean
        public GenesysFilterService genesysFilterService() {
            return new GenesysFilterServiceImpl();
        }

        @Bean
        public AccessionController accessionController() {
            return new AccessionController();
        }

        @Bean
        public GenesysRESTService genesysRESTService() {
            return new GenesysRESTServiceImpl();
        }

        @Bean
        public ElasticService elasticService() {
            return new ElasticsearchSearchServiceImpl();
        }

        @Bean
        public FilterHandler filterHandler() {
            return new FilterHandler();
        }

        @Bean
        public Jackson2ObjectMapperFactoryBean objectMapper() {
            final Jackson2ObjectMapperFactoryBean mapperFactoryBean = new Jackson2ObjectMapperFactoryBean();
            mapperFactoryBean.setFeaturesToDisable(SerializationFeature.FAIL_ON_EMPTY_BEANS, DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
            return mapperFactoryBean;
        }
    }

    @Autowired
    AccessionHistoricRepository accessionHistoricRepository;

    @Autowired
    CropRuleRepository cropRuleRepository;

    @Autowired
    TaxonomyService taxonomyService;

    @Autowired Taxonomy2Repository taxonomy2Repository;

    @Autowired
    MetadataMethodRepository metadataMethodRepository;

    @Autowired
    MetadataRepository metadataRepository;

    @Autowired
    DatasetService datasetService;

    @Autowired
    ObservationRepository observationRepository;

    @Autowired
    KPIParameterRepository kpiParameterRepository;

    @Autowired
    KPIService kpiService;
//    @Autowired
//    OAuthManagementController oAuthManagementController;

    @Autowired
    OAuthAccessTokenPersistence accessTokenPersistence;

    @Autowired
    OAuthRefreshTokenPersistence refreshTokenPersistence;


    @Autowired
    OAuthClientDetailsPersistence clientDetailsPersistence;

    @Autowired
    OAuth2ClientDetailsService clientDetailsService;

    @Autowired
    TokenController tokenController;

    @Autowired
    ConsumerTokenServices tokenServices;

    @Autowired
    TokenStore tokenStore;

    @Autowired
    ConsumerTokenServices consumerTokenServices;

    @Autowired
    EasySMTA easySMTAConnector;

    @Autowired
    JavaMailSender mailSender;

    @Autowired
    AccessionRepository accessionRepository;

    @Autowired
    AccessionCustomRepository accessionCustomRepository;

    @Autowired
    PermissionController permissionController;

    @Autowired
    GeoService geoService;

    @Autowired
    CountryRepository countryRepository;

    @Autowired
    FaoInstituteRepository instituteRepository;

    @Autowired
    FaoInstituteSettingRepository instituteSettingRepository;

    @Autowired
    MethodRepository methodRepository;

    @Autowired
    LookupController lookupController;

    @Autowired
    TraitsController traitsController;

    @Autowired
    @Qualifier("traitServiceMock")
    TraitService traitService;

    @Autowired
    ArticleRepository articleRepository;

    @Autowired
    ContentService contentService;

    @Autowired
    JavaMailSender javaMailSender;

    @Autowired
    UsersController restUsersController;

    @Autowired
    UserService userService;

    @Autowired
    UserController userController;

    @Autowired
    UserPersistence userPersistence;

    @Autowired
    InstituteService instituteService;

    @Autowired
    TeamRepository teamRepository;

    @Autowired
    AclService aclService;

    @Autowired
    AclSidPersistence aclSidPersistence;

    @Autowired
    AclEntryPersistence aclEntryPersistence;

    @Autowired
    AclObjectIdentityPersistence aclObjectIdentityPersistence;

    @Autowired
    AclClassPersistence aclClassPersistence;

    @Autowired
    TeamService teamService;

    @Autowired
    GenesysService genesysService;

    @Autowired
    CropService cropService;

    @Autowired
    CropRepository cropRepository;

    @Autowired
    ParameterRepository parameterRepository;

    @Autowired
    ParameterCategoryRepository parameterCategoryRepository;

    @Autowired
    OrganizationService organizationService;

    @Autowired
    OrganizationRepository organizationRepository;

    @Autowired
    RequestService requestService;

    @Autowired
    MaterialRequestRepository materialRequestRepository;

    @Autowired
    MaterialSubRequestRepository materialSubRequestRepository;
}

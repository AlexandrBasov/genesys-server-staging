#-------------------------------------------------------------------------------
# Copyright 2014 Global Crop Diversity Trust
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#-------------------------------------------------------------------------------

# Errors
http-error.401=Неавторизовано
http-error.401.text=Требуется авторизация
http-error.403=Доступ запрещен
http-error.403.text=У вас нет разрешения на доступ к ресурсу.
http-error.404=Не найдено
http-error.404.text=Запрашиваемый ресурс не найден, но может снова стать доступным в будущем.
http-error.500=Внутренняя ошибка сервера
http-error.500.text=Неожиданное состояние без конкретного соответствующего случаю сообщения.
http-error.503=Сервис недоступен
http-error.503.text=Сервер в настоящий момент недоступен (из-за перегрузки или из-за остановки на обслуживание).


# Login
login.username=Имя пользователя
login.password=Пароль
login.invalid-credentials=Неверные данные для входа.
login.remember-me=Запомнить меня
login.login-button=Вход
login.register-now=Создать учетную запись
logout=Выход
login.forgot-password=Забыли пароль
login.with-google-plus=Вход через учетную запись в Google+

# Registration
registration.page.title=Создать пользователя
registration.title=Создать учетную запись
registration.invalid-credentials=Неверные данные для входа.
registration.user-exists=Имя пользователя уже занято.
registration.email=Адрес электронной почты
registration.password=Пароль
registration.confirm-password=Повторите пароль
registration.full-name=Ваше полное имя
registration.create-account=Создать учетную запись
captcha.text=Текст капчи


id=ID

name=Название
description=Описание
actions=Действия
add=Добавить
edit=Редактировать
save=Сохранить
create=Создать
cancel=Отменить
delete=Удалить

jump-to-top=Вернуться наверх\!

pagination.next-page=Далее >
pagination.previous-page=< Назад


# Language
locale.language.change=Изменить язык
i18n.content-not-translated=Эта информация недоступна на вашем языке. Свяжитесь с нами, если можете помочь с ее переводом\!

data.error.404=Запрошенные Вами данные не были найдены в нашей системе.
page.rendertime=Обработка страницы заняла {0}мс.

footer.copyright-statement=&copy; 2013 - 2015 Поставщики данных и компания Crop Trust

menu.home=На главную 
menu.browse=Просмотреть
menu.datasets=Данные по характеристике и оценке
menu.descriptors=Дескрипторы
menu.countries=Страны
menu.institutes=Институты
menu.my-list=Мой список
menu.about=О проекте Genesys
menu.contact=Свяжитесь с нами
menu.disclaimer=Отказ от ответственности
menu.feedback=Оставить отзыв
menu.help=Помощь
menu.terms=Условия использования
menu.copying=Лицензионное соглашение
menu.privacy=Соглашение о неразглашении личных данных
menu.newsletter=Рассылка новостей Genesys
menu.join-the-community=Присоединяйтесь к сообществу Genesys
menu.faq=Часто задаваемые вопросы
menu.news=Новости
page.news.title=Новости
page.news.intro=Последние новости сообщества Genesys — от подробной информации о новых членах до обновлений сведений об образцах, собранных по всему миру.

# Extra content
menu.what-is-genesys=Что такое Genesys?
menu.who-uses-genesys=Кто использует проект Genesys?
menu.how-to-use-genesys=Как пользоваться проектом Genesys?
menu.history-of-genesys=История проекта Genesys
menu.about-genesys-data=О данных в проекте Genesys



page.home.title=PGR Genesys

user.pulldown.administration=Администрирование
user.pulldown.users=Список пользователя
user.pulldown.logout=Выход
user.pulldown.profile=Мой профиль
user.pulldown.oauth-clients=Клиенты OAuth
user.pulldown.teams=Группы
user.pulldown.manage-content=Управление материалами на сайте

user.pulldown.heading={0}
user.create-new-account=Создать учетную запись
user.full-name=Полное имя
user.email=Адрес электронной почты
user.account-status=Статус учетной записи
user.account-disabled=Учетная запись заблокирована
user.account-locked-until=Учетная запись заблокирована до
user.roles=Роли пользователя
userprofile.page.title=Профиль пользователя
userprofile.page.intro=Успех сообщества Genesys зиждется на его пользователях. Все равно, представляете ли вы крупную организацию или являетесь самостоятельным исследователем — вы можете обновлять свой профиль так, чтобы он отражал ваши интересы и данные вашей коллекции.
userprofile.update.title=Обновить профиль

user.page.list.title=Учетные записи зарегистрированных пользователей

crop.croplist=Список культур
crop.all-crops=Все культуры
crop.page.profile.title=Профиль {0}
crop.taxonomy-rules=Правила классификации
crop.view-descriptors=Просмотреть дескрипторы культуры...

activity.recent-activity=Недавняя активность

country.page.profile.title=Профиль страны\:{0}
country.page.list.title=Список стран
country.page.list.intro=Нажав на название страны, можно отыскать зарегистрированные институты и организации, относящиеся к данной стране, а также статистику растительных генетических ресурсов по всей стране в целом.
country.page.not-current=Эта запись является исторической.
country.page.faoInstitutes={0} институтов зарегистрированы во WIEWS
country.stat.countByLocation={0} образцов в институтах из этой страны
country.stat.countByOrigin={0} образцов, зарегистрированных в Genesys, из этой страны
country.statistics=Статистика страны
country.accessions.from=Образцы собраны в {0}
country.more-information=Подробнее\:
country.replaced-by=Код страны заменен на\: {0}
country.is-itpgrfa-contractingParty={0} является участником Международного Договора о Растительных Генетических Ресурсах для Производства Продовольствия и Ведения Сельского Хозяйства (ITPGRFA/МДГРРПСХ).
select-country=Выберите страну

faoInstitutes.page.list.title=Институты WIEWS
faoInstitutes.page.profile.title=WIEWS {0}
faoInstitutes.stat.accessionCount=Образцы в Genesys\: {0}.
faoInstitutes.stat.datasetCount=Дополнительные наборы данных в Genesys\: {0}.
faoInstitute.stat-by-crop=Самые распространенные культуры
faoInstitute.stat-by-genus=Самые распространенные сорта
faoInstitute.stat-by-species=Самые распространенные виды
faoInstitute.accessionCount={0} образцов
faoInstitute.statistics=Статистика института
faoInstitutes.page.data.title=Образцы в {0}
faoInstitute.accessions.at=Образцы в {0}
faoInstitutes.viewAll=Все зарегистрированные институты
faoInstitutes.viewActiveOnly=Все институты с доступом к Genesys
faoInstitute.no-accessions-registered=Свяжитесь с нами, если вы можете помочь в получении данных из этого института.
faoInstitute.institute-not-current=Запись архивирована.
faoInstitute.view-current-institute=Перейти к {0}
faoInstitute.country=Страна
faoInstitute.code=Код WIEWS
faoInstitute.email=Адрес электронной почты для связи
faoInstitute.acronym=Аббревиатура
faoInstitute.url=Веб-ссылка
faoInstitute.summary=Сводка (метаданные HTML)
faoInstitute.member-of-organizations-and-networks=Организации и сети\:
faoInstitute.uniqueAcceNumbs.true=Для данного института каждый номер образца уникален
faoInstitute.uniqueAcceNumbs.false=Один и тот же номер образца может использоваться в различных коллекциях данного института.
faoInstitute.requests.mailto=Адрес электронной почты для подачи заявок на материалы\:
faoInstitute.allow.requests=Разрешить заявку на материалы
faoInstitute.sgsv=Код SGSV
faoInstitute.data-title=База данных с информацией об образцах, хранящихся в  {0}
faoInstitute.overview-title=Обзор информации об образцах, хранящихся в {0}
faoInstitute.datasets-title=Данные описания и оценки, предоставленные {0}
faoInstitute.meta-description=Обзор растительных генетических ресурсов, хранящихся в коллекциях в генном банке {0} ({1})
faoInstitute.link-species-data=Просмотр информации по {2} образцам в {0} ({1})
faoInstitute.wiewsLink={0} подробностей на веб-сайте FAO WIEWS

view.accessions=Просмотр образцов
view.datasets=Просмотр наборов данных
paged.pageOfPages=Страница {0} из {1}
paged.totalElements={0} записей

accessions.number={0} образцов
accession.metadatas=Данные по характеристике и оценке
accession.methods=Данные по характеристике и оценке
unit-of-measure=Единица измерения

ce.trait=Особенность
ce.sameAs=Те же, что
ce.methods=Методы
ce.method=Метод
method.fieldName=Поле БД

accession.uuid=УУИД
accession.accessionName=Номер образца
accession.origin=Страна происхождения
accession.holdingInstitute=Институт-держатель
accession.holdingCountry=Местоположение
accession.taxonomy=Научное название
accession.crop=Название культуры
accession.otherNames=Также известен как
accession.inTrust=На попечении
accession.mlsStatus=Статус MLS
accession.duplSite=Институт защитного дублирования
accession.inSvalbard=Копия для сохранности в хранилище семян на Шпицбергене.
accession.inTrust.true=Этот образец подпадает под статью 15 международного договора о растительных генетических ресурсах для производства продовольствия и ведения сельского хозяйства.
accession.mlsStatus.true=Этот образец принадлежит Многосторонней системе МДГРРПСХ.
accession.inSvalbard.true=Копия для увеличения сохранности во Всемирном хранилище семян на Шпицбергене.
accession.not-available-for-distribution=Образец НЕДОСТУПЕН для распространения 
accession.this-is-a-historic-entry=Это запись об истории образца
accession.historic=Запись об истории
accession.available-for-distribution=Образец доступен для распространения 
accession.elevation=Высота над уровнем моря
accession.geolocation=Координаты (широта, долгота)

accession.storage=Тип хранилища идиоплазмы
accession.storage.=
accession.storage.10=Коллекция семян
accession.storage.11=Коллекция семян краткосрочного хранения
accession.storage.12=Коллекция семян со средним сроком хранения
accession.storage.13=Коллекция семян длительного хранения
accession.storage.20=Полевая колллекция
accession.storage.30=Коллекция in vitro
accession.storage.40=Коллекция с замораживанием
accession.storage.50=Коллекция ДНК
accession.storage.99=Прочее

accession.breeding=Информация о селекционере
accession.breederCode=Код селекционера
accession.pedigree=Происхождение
accession.collecting=Информация о сборе
accession.collecting.site=Местоположение участка сбора
accession.collecting.institute=Собирающий институт
accession.collecting.number=Номер сбора
accession.collecting.date=Дата сбора образца
accession.collecting.mission=ID миссии сбора
accession.collecting.source=Источник сбора/получения

accession.collectingSource.=
accession.collectingSource.10=Область обитания в диком виде
accession.collectingSource.11=Леса
accession.collectingSource.12=Кустарниковые заросли
accession.collectingSource.13=Пастбищные угодья
accession.collectingSource.14=Пустыня или тундра
accession.collectingSource.15=Водный обитетель
accession.collectingSource.20=Полевое или культурное растение
accession.collectingSource.21=Поле
accession.collectingSource.22=Сад
accession.collectingSource.23=Приусадебное хозяйство (городское, пригородное или сельское)
accession.collectingSource.24=Земли под паром
accession.collectingSource.25=Пастбища
accession.collectingSource.26=Амбар
accession.collectingSource.27=Гумно
accession.collectingSource.28=Парк, усадьба
accession.collectingSource.30=Рынок или магазин
accession.collectingSource.40=Институт, экспериментальная станция, исследовательская организация, банк генов
accession.collectingSource.50=Компания — производитель семян
accession.collectingSource.60=Сорняк, обитатель сельской местности или нарушенных местностей
accession.collectingSource.61=Обочина
accession.collectingSource.62=Кромка поля
accession.collectingSource.99=Другое

accession.donor.institute=Институт-донор
accession.donor.accessionNumber=ID донорского образца
accession.geo=Географическая инфрмация
accession.geo.datum=Информация о координатах
accession.geo.method=Метод привязки к местности
accession.geo.uncertainty=Неопределенность координат
accession.sampleStatus=Биологический статус образца

accession.sampleStatus.=
accession.sampleStatus.100=Дикорастущий
accession.sampleStatus.110=Природный
accession.sampleStatus.120=Полудикий/полуестественный
accession.sampleStatus.130=Полудикие растения/насаждения
accession.sampleStatus.200=Сорное
accession.sampleStatus.300=Традиционный/местный сорт
accession.sampleStatus.400=Селекционный/исследовательский материал
accession.sampleStatus.410=Селекционеры
accession.sampleStatus.411=Синтетическое население
accession.sampleStatus.412=Гибрид
accession.sampleStatus.413=Основная популяция
accession.sampleStatus.414=Инбредная линия
accession.sampleStatus.415=Расщепляющаяся популяция
accession.sampleStatus.416=Селекция клонированием
accession.sampleStatus.420=Генетический фонд
accession.sampleStatus.421=Мутант
accession.sampleStatus.422=Цитогенетические банки данных
accession.sampleStatus.423=Другие генетические банки данных
accession.sampleStatus.500=Улучшенный сорт
accession.sampleStatus.600=ГМО
accession.sampleStatus.999=Другое

accession.availability=Доступность для распространения
accession.aliasType.ACCENAME=Название образца
accession.aliasType.DONORNUMB=Идентификатор донорского образца
accession.aliasType.BREDNUMB=Название, присвоенное селекционером
accession.aliasType.COLLNUMB=Номер сбора
accession.aliasType.OTHERNUMB=Другие названия
accession.aliasType.DATABASEID=(ID базы данных)
accession.aliasType.LOCALNAME=(Местное название)

accession.availability.=Нет данных
accession.availability.true=Доступно
accession.availability.false=Недоступно

accession.historic.true=История
accession.historic.false=Активно
accession.acceUrl=URL дополнительного образца

accession.page.profile.title=Профиль образца\: {0}
accession.page.resolve.title=Найдено несколько образцов
accession.resolve=В Genesys найдено несколько образцов с именем ''{0}''. Выберите один из списка.
accession.page.data.title=Браузер для просмотра образцов
accession.page.data.intro=Изучайте данные об образцах вместе с проектом Genesys. ЧТобы сузить поиск, пользуйтесь ильтрами. такаими как страна происхождения, тип и вид сельскохозяйственных растений или широта и долгота участка сбора образцов.
accession.taxonomy-at-institute=Просмотреть {0} в {1}

accession.svalbard-data=Данные дублирования во Всемирном семенохранилище на Шпицбергене
accession.svalbard-data.taxonomy=Известное в литературе научное название
accession.svalbard-data.depositDate=Дата размещения на хранение
accession.svalbard-data.boxNumber=Номер ячейки
accession.svalbard-data.quantity=Количество
accession.remarks=Примечания

taxonomy.genus=Сорт
taxonomy.species=Виды
taxonomy.taxonName=Научное название
taxonomy-list=Список классификаций

selection.page.title=Выбранные образцы
selection.page.intro=По мере изучения миллионов образцов, собранных в проекте Genesys, вы сможете создать свой собственный список, который поможет отслеживать результаты поиска. Выбранные вами результаты будут сохранены здесь, так что вы сможете в любой момент вернуться к ним.
selection.add=Добавить {0} к списку
selection.remove=Удалить {0} из списка
selection.clear=Очистить список
selection.reload-list=Перезагрузить список
selection.map=Показать карту мест сбора образцов
selection.send-request=Направить заявку на получение идиоплазмы
selection.empty-list-warning=Вы не добавили образцы в список
selection.add-many=Проверить и добавить
selection.add-many.accessionIds=Укажите ID образцов, принятые в Genesys, разделяя их пробелом.

savedmaps=Запомнить текущую карту
savedmaps.list=Список карт
savedmaps.save=Запомнить карту

filter.enter.title=Ввести название фильтра
filters.page.title=Фильтры данных
filters.view=Текущие фильтры
filter.filters-applied=Вы применили фильтры.
filter.filters-not-applied=Вы можете отфильтровать данные.
filters.data-is-filtered=К данным применен фильтр.
filters.toggle-filters=Фильтры
filter.taxonomy=Научное название
filter.art15=Образец в соответствии со Ст. 15 МДГРРПСХ
filter.acceNumb=Учетный номер
filter.alias=Название образца
filter.crops=Название культуры
filter.orgCty.iso3=Страна происхождения
filter.institute.code=Имя института-владельца
filter.institute.country.iso3=Страна, в которой располагается институт-владелец
filter.sampStat=Биологический статус образца
filter.institute.code=Имя института-владельца
filter.geo.latitude=Широта
filter.geo.longitude=Долгота
filter.geo.elevation=Высота над уровнем моря
filter.taxonomy.genus=Род
filter.taxonomy.species=Виды
filter.taxonomy.subtaxa=Субтаксоны
filter.taxSpecies=Виды
filter.taxonomy.sciName=Научное название
filter.sgsv=Копия для сохранности в хранилище семян на Шпицбергене.
filter.mlsStatus=Статус образца в MLS
filter.available=Доступен для распространения
filter.historic=Историческая запись
filter.donorCode=Институт-донор
filter.duplSite=Местоположение организации, осуществляющей защитное дублирование
filter.download-dwca=Скачать ZIP
filter.download-mcpd=Скачать MCPD
filter.add=Добавить фильтр
filter.additional=Дополнительные фильтры
filter.apply=Применить
filter.close=Закрыть
filter.remove=Удалить фильтр
filter.autocomplete-placeholder=Введите более 3 символов...
filter.coll.collMissId=ID миссии сбора
filter.storage=Тип хранилища идиоплазмы
filter.string.equals=Равные
filter.string.like=Начинается с
filter.inverse=Не включая
filter.set-inverse=Выбранные значения не включаются


search.page.title=Полнотекстовый поиск
search.no-results=Не найдено данных, соответствующих вашему запросу.
search.input.placeholder=Поиск
search.search-query-missing=Введите поисковый запрос
search.search-query-failed=Извините, поиск безрезультатный, ошибка {0}
search.button.label=Поиск
search.add-genesys-opensearch=Зарегистрировать поиск в проекте Genesys в вашем браузере

admin.page.title=Администрирование Genesys 2
metadata.page.title=Наборы данных
metadata.page.intro=Изучайте наборы характеристических и оценочных данных, выложенные в проект Genesys отдельными исследователями и исследовательскими организациями. Каждый из таких наборов можно скачать в формате Excel и CSV.
metadata.page.view.title=О наборе данных
metadata.download-dwca=Скачать ZIP
page.login=Вход

traits.page.title=Дескрипторы
trait-list=Дескрипторы


organization.page.list.title=Организации и сети
organization.page.profile.title={0}
organization.slug=Сокращенное название организации или сети
organization.title=Полное наимнование
organization.summary=Сводка (метаданные HTML)
filter.institute.networks=Сеть


menu.report-an-issue=Сообщить об ошибке
menu.scm=Исходный код
menu.translate=Перевод Genesys

article.edit-article=Редактирование статьи
article.slug=Колонтитул статьи (URL)
article.title=Название статьи
article.body=Основной текст статьи
article.summary=Сводка (метаданные HTML)
article.post-to-transifex=Разместить в Transifex
article.remove-from-transifex=Удалить из Transifex
article.fetch-from-transifex=Получить перевод
article.transifex-resource-updated=Ресурс на Transifex был успешно обновлен.
article.transifex-resource-removed=Ресурс был успешно удален из Transifex.
article.transifex-failed=При обмене данными с системой Transifex произошла ошибка
article.translations-updated=Ресурс успешно обновлен данными с переводом
article.share=Поделиться статьей

activitypost=Статус
activitypost.add-new-post=Добавить новый пост
activitypost.post-title=Название поста
activitypost.post-body=Тело

blurp.admin-no-blurp-here=Blurp отсутствтует.
blurp.blurp-title=Название blurp
blurp.blurp-body=Содержание blurp
blurp.update-blurp=Сохранить blurp


oauth2.confirm-request=Подтвердить доступ
oauth2.confirm-client=Вы, <b>{0}</b>, открываете доступ для <b>{1}</b> к вашим защищенным данным.
oauth2.button-approve=Да, разрешить доступ
oauth2.button-deny=Нет, отказать в доступе

oauth2.authorization-code=Код авторизации
oauth2.authorization-code-instructions=Скопируйте этот код авторизации\:

oauth2.access-denied=Доступ запрещен
oauth2.access-denied-text=Вы запретили доступ к своим данным.

oauth-client.page.list.title=Клиенты OAuth2
oauth-client.page.profile.title=Клиент OAuth2\: {0}
oauth-client.active-tokens=Список выданных маркеров
client.details.title=Должность клиента
client.details.description=Описание

maps.loading-map=Идет загрузка карты...
maps.view-map=Просмотреть карту
maps.accession-map=Карта образцов
maps.accession-map.intro=Карта показывает места сбора тех образов, для которых имеется привязка к местности.

audit.createdBy=Создано {0}
audit.lastModifiedBy=Последнее обновление произведено {0}

itpgrfa.page.list.title=Участники МДГРРПСХ

request.page.title=Идет запрос материала из институтов-хранилищ
request.total-vs-available=Известно, что из {0} указанных образцов {1} доступно для распространения.
request.start-request=Запросить доступные образцы идиоплазмы
request.your-email=Адрес вашей электронной почты\:
request.accept-smta=Принятие условий SMTA/MTA\:
request.smta-will-accept=Я принимаю условия и положения SMTA/MTA
request.smta-will-not-accept=Я НЕ принимаю положения и условия SMTA/MTA
request.smta-not-accepted=Вы не указали, что принимаете положения и условия SMTA/MTA. Ваша заявка на материалы обрабатываться не будет.
request.purpose=Укажите, для чего будет использоваться материал\:
request.purpose.0=Прочее (просьба подробнее описать в поле «Примечания»)
request.purpose.1=Исследования в области пишевой промышленности и сельского хозяйства
request.notes=Любые дополнительные комментарии и примечания можно написать здесь\:
request.validate-request=Подтвердите свою заявку
request.confirm-request=Подтвердите получение заявки
request.validation-key=Пароль подтверждения\:
request.button-validate=Подвердить
validate.no-such-key=Пароль подтверждения недействителен

team.user-teams=Группы пользователя
team.create-new-team=Создать новую команду
team.team-name=Название команды
team.leave-team=Покинуть команду
team.team-members=Члены команды
team.page.profile.title=Команда\: {0}
team.page.list.title=Все команды
validate.email.key=Введите пароль
validate.email=Валидация адреса электронной почты
validate.email.invalid.key=Пароль неверный

edit-acl=Редактировать разрешения
acl.page.permission-manager=Менеджер разрешений
acl.sid=Идентификатор безопасности
acl.owner=Владелец объекта
acl.permission.1=Считывать
acl.permission.2=Записать
acl.permission.4=Создать
acl.permission.8=Удалить
acl.permission.16=Управлять


ga.tracker-code=Код трекера GA

boolean.true=Да
boolean.false=Нет
boolean.null=Неизвестно
userprofile.password=Сброс пароля
userprofile.enter.email=Введите Ваш адрес электронной почты
userprofile.enter.password=Введите новый пароль
userprofile.email.send=Отправить электронное письмо

verification.invalid-key=Пароль токена недействителен
verification.token-key=Ключ валидации
login.invalid-token=Недействительный токен доступа

descriptor.category=Категория дескриптора
method.coding-table=Таблица кодирования

oauth-client.info=Информация о клиенте
oauth-client.list=Список клиентов OAuth
client.details.client.id=ID подробных сведений о клиенте
client.details.additional.info=Дополнительная информация
client.details.token.list=Список токенов
client.details.refresh-token.list=Список обновленных токенов
oauth-client.remove=Удалить
oauth-client.remove.all=Удалить все
oauth-client=Клиент
oauth-client.token.issue.date=Дата создания
oauth-client.expires.date=Дата истечения срока действия
oauth-client.issued.tokens=Созданные токены
client.details.add=Добавить клиента в OAuth
oauth-client.create=Создать клиента OAuth
oauth-client.id=ID клиента
oauth-client.secret=Секретное слово клиента
oauth-client.redirect.uri=URL перенаправления клиента
oauth-client.access-token.accessTokenValiditySeconds=Действительность токена для доступа
oauth-client.access-token.refreshTokenValiditySeconds=Обновить действительность токена
oauth-client.access-token.defaultDuration=Использовать значение, принятое по умолчанию
oauth-client.title=Должность клиента
oauth-client.description=Описание
oauth2.error.invalid_client=Недействительный ID клиента. Проверьте параметры client_id и client_secret

team.user.enter.email=Введите адрес электронной почты пользователя
user.not.found=Пользователь не найден
team.profile.update.title=Обновить информацию о команде

autocomplete.genus=Найти род

stats.number-of-countries={0} Стран
stats.number-of-institutes={0} Институтов
stats.number-of-accessions={0} Образцов

navigate.back=\\u21E0 Back


content.page.list.title=Список статей
article.lang=Язык
article.classPk=Объект

session.expiry-warning-title=Предупреждение о том. что срок сессии истек
session.expiry-warning=Срок вашей нынешней сессии скоро истечет. Хотите продолжить сессию?
session.expiry-extend=Продолжить сессию

download.kml=Скачать KML

data-overview=Статистический обзор
data-overview.intro=Статистический обзор информации, хранящейся в Genesys, с учетом примененных в настоящий момент фильтров — от общего количества образцов, накопленного в каждой из стран, до разнообразия коллекций.
data-overview.short=Обзор
data-overview.institutes=Институты-держатели
data-overview.composition=Состав активов банка генетических данных
data-overview.sources=Источники материала
data-overview.management=Упралвение сбором
data-overview.availability=Наличие материала
data-overview.otherCount=Другое
data-overview.missingCount=Не указано
data-overview.totalCount=Всего
data-overview.donorCode=Код института-донора по FAO WIEWS
data-overview.mlsStatus=Доступно для распространения согласно MLS


admin.cache.page.title=Обзор кэша приложения
cache.stat.map.ownedEntryCount=Собственные записи в кэше
cache.stat.map.lockedEntryCount=Блокированные записи в кэше
cache.stat.map.puts=Количество записей, помещаемых в кэш
cache.stat.map.hits=Количество совпадений в кэше

loggers.list.page=Список сконфигурированных регистраторов
logger.add-logger=Добавить регистратора
logger.edit.page=Страница конфигурации регистрации
logger.name=Название регистратора
logger.log-level=Уровень журнала
logger.appenders=Аппендер журнала

menu.admin.index=Администратор
menu.admin.loggers=Регистраторы
menu.admin.caches=Кэши
menu.admin.ds2=Наборы данных DS2

worldclim.monthly.title=Климат на участке сбора образцов
worldclim.monthly.precipitation.title=Ежемесячное количество осадков
worldclim.monthly.temperatures.title=Среднемесячная температура
worldclim.monthly.precipitation=Ежемесячное количество осадков [мм]
worldclim.monthly.tempMin=Минимальная температура [°C]
worldclim.monthly.tempMean=Средняя температура [°C]
worldclim.monthly.tempMax=Максимальная температура [°C]

worldclim.other-climate=Другие климатические данные

download.page.title=До того, как начать скачивание
download.download-now=Начните скачивание, я подожду

resolver.page.index.title=Найти постоянный идентификатор
resolver.acceNumb=Номер образца
resolver.instCode=Код института, владеющего образцом
resolver.lookup=Найти идентификатор
resolver.uuid=УУИД
resolver.resolve=Разрешить вопрос

resolver.page.reverse.title=Результат разрешения
accession.purl=Постоянный URL

menu.admin.kpi=КПЭ
admin.kpi.index.page=КПЭ
admin.kpi.execution.page=Осуществление КПЭ
admin.kpi.executionrun.page=Подробные данные о цикле исполнения

accession.pdci=Показатель полноты паспортных данных
accession.pdci.jumbo=\ Баллы PDCI\: {0,number,0.00} из 10.0
accession.pdci.institute-avg=Средний балл PDCI для данного института составляет {0,number,0.00}
accession.pdci.about-link=Читать о показателе полноты паспортных данных
accession.pdci.independent-items=Вне зависимости от типа популяции
accession.pdci.dependent-items=В зависимости от типа популяции
accession.pdci.stats-text=Средний балл PDCI для {0} образцов составляет {1,number,0.00}, при этом минимальный балл равен {2,number,0.00} и максимальный балл равен {3,number,0.00}.

accession.donorNumb=Код института-донора
accession.acqDate=Дата сбора образца

accession.svalbard-data.url=URL базы данных Svalbard
accession.svalbard-data.url-title=Информация о депозите в базе данных SGSV
accession.svalbard-data.url-text=Просмотреть информацию о депозите в SGSV для {0}

filter.download-pdci=Скачать данные PDCI
statistics.phenotypic.stats-text=Из {0} образцов {1} образцов ({2,number,0.##%}) имеют по меньшей мере одну дополнительную характеристику, внесенную в базу данных Genesys (из среднего количества {3,number,0.##} характеристик в {4,number,0.##} базах данных).

twitter.tweet-this=Пишите в Twitter\!
twitter.follow-X=Следуйте за нами по адресу @
linkedin.share-this=Напишите в Linkedin

welcome.read-more=Расширьте знания о проекте Genesys
twitter.latest-on-twitter=Последние новости в Twitter
video.play-video=Просмотр видео

heading.general-info=Общая информация
heading.about=О проекте
heading.see-also=См. также
see-also.map=Карта расположения мест сбора образцов
see-also.overview=Обзор коллекции
see-also.country=Дополнительные сведения о PRG-данных, сохраненных в {0}

# help.page.intro=Visit the tutorials section to learn how to use Genesys.

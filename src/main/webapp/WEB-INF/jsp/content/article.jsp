<!DOCTYPE html>

<%@include file="/WEB-INF/jsp/init.jsp"%>

<html>
<head>
<title>${title}</title>
<local:content-headers description="${jspHelper.htmlToText(article.summary, 150)}" title="${title}" />
</head>
<body>
	<content tag="header">
	<div class="hidden-xs top-image">
		<img src="<c:url value="/html/images/aim/banner-${jspHelper.randomInt(1, 4)}.jpg" />" title="${title}" />
	</div>
	<div class="top-title">
		<div class="banner-<c:out value="${article.slug}" />"></div>
		<c:if test="${title ne ''}">
			<div class="container">
				<security:authorize access="hasRole('ADMINISTRATOR') or hasRole('CONTENTMANAGER')">
					<form class="inline-form pull-right" method="post" action="<c:url value="/content/transifex"/>">
						<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
						<input id="articleSlug" type="hidden" name="slug" value="${article.slug}" />

						<a href="<c:url value="/content/${article.slug}/edit" />" class="btn btn-default">
							<spring:message code="edit" />
						</a>
					
						<input type="submit" name="fetch-all" class="btn btn-default" value="<spring:message code="article.fetch-from-transifex" />" />
					</form>
				</security:authorize>
				<h1>
					<c:out value="${title}" escapeXml="false" />
				</h1>
			</div>
		</c:if>
	</div>
	</content>

	<c:if test="${article.lang != pageContext.response.locale.language}">
		<%@include file="/WEB-INF/jsp/not-translated.jsp"%>
	</c:if>

	<%@include file="transifex-responses.jsp"%>

	<div class="article">
		<div class="right-side main-col col-md-9">

			<div class="free-text" dir="${article.lang=='fa' || article.lang=='ar' ? 'rtl' : 'ltr'}">
				<c:out value="${article.body}" escapeXml="false" />
			</div>

			<div class="article-timestamp">
				<local:prettyTime date="${article.postDate.time}" locale="${pageContext.response.locale}" />
			</div>

			<div class="share-article">
				<p>
					<spring:message code="article.share" />
				</p>
				<ul class="list-inline">
					<%--
					<li class="envelope">
						<a href="">
							<i class="fa fa-envelope-o"></i>
						</a>
					</li>
 					--%>
					<li class="twitter">
						<local:tweet text="${article.title}" hashTags="GenesysPGR" />
					</li>
					<li class="linkedin">
						<local:linkedin-share text="${article.title}" summary="${article.body}" />
					</li>
				</ul>
			</div>

			<%-- <div class="see-also">
				<h3>
					<span>
						<spring:message code="heading.see-also" />
					</span>
				</h3>
				<div class="row">
					<div class="col-xs-12 col-sm-4">
						<div class="see-also-block" style="background-image: url('<c:url value="/html/images/pic_news_1.png" />')">
							<h2>History of Genesys</h2>
						</div>
					</div>
					<div class="col-xs-12 col-sm-4">
						<div class="see-also-block" style="background-image: url('<c:url value="/html/images/pic_news_1.png" />')">
							<h2>How to use Genesys</h2>
						</div>
					</div>
					<div class="col-xs-12 col-sm-4">
						<div class="see-also-block" style="background-image: url('<c:url value="/html/images/pic_news_1.png" />')">
							<h2>About Genesys Data 123 </h2>
						</div>
					</div>
				</div>
			</div> --%>
		</div>


		<div class="col-md-3 sidebar-nav col-xs-12">
			<cms:menu key="${menu.key}" items="${menu.items}" />
		</div>
	</div>

<content tag="javascript">
<script type="text/javascript">
  $(document).ready(function() {
    $('a[href="' + window.location.pathname + '"]').parent().addClass('active-link');
  });
</script>
</content>

</body>
</html>
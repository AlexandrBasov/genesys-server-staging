<!DOCTYPE html>

<%@include file="/WEB-INF/jsp/init.jsp"%>

<html>
<head>
<title><spring:message code="oauth-client.page.list.title" /></title>
</head>
<body>
	<h1>
		<spring:message code="oauth-client.page.list.title" />
	</h1>

	<ul class="funny-list">
		<c:forEach items="${items}" var="item" varStatus="status">
			<li class="${status.count % 2 == 0 ? 'even' : 'odd'}"><a href="<c:url value="/admin/oauth/${item.clientId}" />"><c:out value="${item.clientId}" /></a></li>
		</c:forEach>
	</ul>

</body>
</html>
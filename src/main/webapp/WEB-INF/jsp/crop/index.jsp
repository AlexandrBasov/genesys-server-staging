<!DOCTYPE html>

<%@include file="/WEB-INF/jsp/init.jsp"%>

<html>
<head>
<title><spring:message code="crop.page.profile.title" arguments="${crop.getName(pageContext.response.locale)}" /></title>
</head>
<body>
	<c:if test="${crop eq null}">
		<div class="alert alert-error">
			<spring:message code="data.error.404" />
		</div>
	</c:if>

	<h1>
		<c:out value="${crop.getName(pageContext.response.locale)}" />
	</h1>

	<security:authorize access="hasRole('ADMINISTRATOR') or hasPermission(#crop, 'ADMINISTRATION')">
		<a href="<c:url value="/acl/${crop.getClass().name}/${crop.id}/permissions"><c:param name="back">/c/${crop.shortName}</c:param></c:url>" class="close"> <spring:message code="edit-acl" /></a>
	</security:authorize>

	<div class="free-text">
		<c:out value="${crop.getDescription(pageContext.response.locale)}" />
	</div>
	
	<div>
		<a href="<c:url value="/c/${crop.shortName}/data" />"><spring:message code="view.accessions" /></a>
	</div>
	<div>
		<a href="<c:url value="/c/${crop.shortName}/descriptors" />"><spring:message code="crop.view-descriptors" /></a>
	</div>

	<h4><spring:message code="crop.taxonomy-rules" /></h4>
	<ul class="funny-list">
		<c:forEach items="${cropRules}" var="rule">
			<li class="${rule.included ? '' : 'excluded'}" /><b>${rule.included ? '+' : '-'}</b> <c:out value="${rule.genus}" /> <c:out value="${rule.species eq null ? '*' : rule.species}" /></li>
		</c:forEach>
	</ul>

<%-- 
	<div class="free-text">
		<c:out value="${crop.language}" />
	</div>
 --%>
 

	<h3><spring:message code="taxonomy-list" /></h3>

	<ul class="funny-list">
		<c:forEach items="${cropTaxonomies.content}" var="cropTaxonomy" varStatus="status">
			<li class="${status.count % 2 == 0 ? 'even' : 'odd'}"><a href="<c:url value="/acn/t/${cropTaxonomy.taxonomy.genus}/${cropTaxonomy.taxonomy.species}" />"><c:out value="${cropTaxonomy.taxonomy.taxonName}" /></a></li>
		</c:forEach>
		<c:if test="${cropTaxonomies.hasNext()}">
			<li id="loadMoreTaxonomies"><button class="btn">Load more...</button></li>
		</c:if>
	</ul>

<content tag="javascript">	
	<script type="text/javascript">
		jQuery(document).ready(function() {
			$("body").on("click", "#loadMoreTaxonomies", function(event) {
				event.preventDefault();
				var loader=$(this);
				var page=loader.attr("page") || 2;
				$.ajax({
					url : "<c:url value="/c/${crop.shortName}/ajax/taxonomies" />",
					type : "GET",
					data : {
						"page" : page
					},
					success : function(data) {
						loader.before(data);
						loader.attr("page", parseInt(page)+1);
					},
					error: function(err) {
						loader.remove();
					}
				});
			});
		});
	</script>
</content>
</body>
</html>
<!DOCTYPE html>

<%@include file="/WEB-INF/jsp/init.jsp"%>

<html>
<head>
<title><spring:message code="oauth-client.page.profile.title" arguments="${clientDetails.clientId}" /></title>
</head>
<body>
	<h1>
		<spring:message code="oauth-client.page.profile.title" arguments="${clientDetails.clientId}" />
	</h1>

	<form role="form" class="form-horizontal validate" action="<c:url value="/management/save-client" />" method="post">
		<c:if test="${clientDetails != null}">
		<input type="hidden" name="id" value="${clientDetails.id}" />
		</c:if>
		<c:if test="${clientDetails == null}">
		<input type="hidden" name="newClient" value="new" />
		</c:if>
		<div class="form-group">
			<label for="clientId" class="col-lg-2 control-label"><spring:message code="oauth-client.id" /></label>
			<div class="col-lg-10">
				<span class="form-control"><c:out value="${clientDetails.clientId}" /></span>
			</div>
		</div>
		<div class="form-group">
			<label for="secret" class="col-lg-2 control-label"><spring:message code="oauth-client.secret" /></label>
			<div class="col-lg-10">
				<input type="text" name="client_secret" class="form-control" value="<c:out value="${clientDetails.clientSecret}" />" />
			</div>
		</div>
        <div class="form-group">
            <label for="redirectUri" class="col-lg-2 control-label"><spring:message code="oauth-client.title" /></label>
            <div class="col-lg-10">
                <input type="text" name="title" class="form-control" value="<c:out value="${clientDetails.title}" />" />
            </div>
        </div>
        <div class="form-group">
            <label for="redirectUri" class="col-lg-2 control-label"><spring:message code="oauth-client.description" /></label>
            <div class="col-lg-10">
                <textarea name="description" class="form-control"><c:out value="${clientDetails.description}" /></textarea>
            </div>
        </div>
        <div class="form-group">
            <label for="redirectUri" class="col-lg-2 control-label"><spring:message code="oauth-client.redirect.uri" /></label>
            <div class="col-lg-10">
                <textarea id="redirectUri" name="redirectUris" class="form-control"><c:out value="${clientDetails.redirectUris}" /></textarea>
            </div>
        </div>
        <div class="form-group">
            <label for="redirectUri" class="col-lg-2 control-label"><spring:message code="oauth-client.access-token.accessTokenValiditySeconds" /></label>
            <div class="col-lg-10">
                <input type="text" name="accessTokenValiditySeconds" class="form-control" placeholder="<spring:message code="oauth-client.access-token.defaultDuration" />" value="<c:out value="${clientDetails.accessTokenValiditySeconds}" />" />
            </div>
        </div>
         <div class="form-group">
            <label for="redirectUri" class="col-lg-2 control-label"><spring:message code="oauth-client.access-token.refreshTokenValiditySeconds" /></label>
            <div class="col-lg-10">
                <input type="text" name="refreshTokenValiditySeconds" class="form-control" placeholder="<spring:message code="oauth-client.access-token.defaultDuration" />" value="<c:out value="${clientDetails.refreshTokenValiditySeconds}" />" />
            </div>
        </div>
        <%--
        <div class="form-group">
            <label for="redirectUri" class="col-lg-2 control-label"><spring:message code="oauth-client.clientType" /></label>
            <div class="col-lg-10">
                <select id="clientType" name="clientType" class="form-control">
                	<option value="WEBAPP">Web application</option>
                	<option value="SERVICE">Service</option>
                	<option value="PACKAGED">Packaged</option>
                </select>
            </div>
        </div>
        --%>
		<div class="form-group">
			<div class="col-lg-offset-2 col-lg-10">
				<input type="submit" name="action-save" value="<spring:message code="save"/>" class="btn btn-primary" />
				<input type="submit" name="action-delete" value="<spring:message code="delete"/>" class="btn btn-default" />
				<a class="btn btn-default" href="<c:url value="/management/" />" class="btn"> <spring:message code="cancel" />
				</a>
			</div>
		</div>
        <!-- CSRF protection -->
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
	</form>
</body>
</html>
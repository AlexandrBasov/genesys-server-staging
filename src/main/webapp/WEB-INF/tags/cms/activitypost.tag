<%@ tag description="Display menu with menu items" pageEncoding="UTF-8" %>
<%@ tag body-content="empty" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="local" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="cms" tagdir="/WEB-INF/tags/cms"%>
<%@ attribute name="activityPost" required="true" type="org.genesys2.server.model.impl.ActivityPost" %>

<div class="post type-article">
	<div class="post-head clearfix">
		<div class="user-icon">
			<img src="<c:url value="/html/images/icon_user_genesys.png" />" alt="" />
		</div>
		<div class="post-head-content" dir="ltr">
			<c:out value="${activityPost.title}" escapeXml="false" />
		</div>
	</div>

	<c:if test="${activityPost.body ne null and activityPost.body.length() gt 0}">
		<div class="post-inner clearfix">
			<div class="post-content" dir="ltr">
				<c:out value="${activityPost.body}" escapeXml="false" />
			</div>
		</div>
	</c:if>

	<div class="post-actions">
		<security:authorize access="hasRole('ADMINISTRATOR') or hasRole('CONTENTMANAGER')">
			<a href="<c:url value="/content/activitypost/${activityPost.id}/edit" />">
				<spring:message code="edit" />
			</a>
		</security:authorize>

		<local:tweet text="${activityPost.title}" hashTags="GenesysPGR" iconOnly="true" />
		<local:linkedin-share text="${activityPost.title}" summary="${activityPost.body}" iconOnly="true" />

		<%-- <spring:message code="audit.createdBy" arguments="${activityPost.createdBy.name}" /> --%>
		<c:if test="${activityPost.lastModifiedBy ne null}">
			<spring:message code="audit.lastModifiedBy" arguments="${jspHelper.userFullName(activityPost.lastModifiedBy)}" />
		</c:if>
		<local:prettyTime date="${activityPost.postDate.time}" locale="${pageContext.response.locale}" />
	</div>
</div>

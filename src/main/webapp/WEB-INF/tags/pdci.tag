<%@ tag description="Display PDCI" pageEncoding="UTF-8"%>
<%@ tag body-content="tagdependent"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ attribute name="value" required="true" type="org.genesys2.server.model.genesys.PDCI"%>
<%
	
%>
<%-- <h5>
    <spring:message code="accession.pdci.independent-items" />
  </h5> --%>
<div class="row">
  <c:forEach items="${pdci.independentItems}" var="item">
    <div class="col-sm-3 col-xs-5 ${pdci[item] == 0 ? 'pdci-improve' : ''}">${item.toUpperCase()}</div>
    <div class="col-sm-1 col-xs-1">${pdci[item]}</div>
  </c:forEach>
</div>
<h5>
  <spring:message code="accession.pdci.dependent-items" />
</h5>
<div class="row">
  <c:forEach items="${pdci.dependentItems}" var="item">
    <div class="col-sm-3  col-xs-5 ${pdci[item] == 0 ? 'pdci-improve' : ''}">${item.toUpperCase()}</div>
    <div class="col-sm-1 col-xs-1">${pdci[item]}</div>
  </c:forEach>
</div>
<%-- <c:forEach items="${pdci.independentItems}" var="item">
accession.pdci.${item}=<br />
</c:forEach>
<c:forEach items="${pdci.dependentItems}" var="item">
accession.pdci.${item}=<br />
</c:forEach> --%>
<%-- <div class="row">
<c:forEach items="${pdci.pdciItems}" var="item">
"${item}", 
</c:forEach>
</div> --%>

/**
 * Copyright 2015 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.model.genesys;

import java.io.Serializable;
import java.util.List;

/**
 * Institute PDCI statistics
 */
public class PDCIStatistics implements Serializable {

	private static final long serialVersionUID = -3576941362217038857L;
	
	private Float min = null;
	private Float max = null;
	private Double avg = null;

	private long[] histogram = new long[21];
	private Long count;

	public void setMin(Float value) {
		this.min = value;
	}

	public Float getMin() {
		return min;
	}

	public void setMax(Float value) {
		this.max = value;
	}

	public Float getMax() {
		return max;
	}

	public void setAvg(Double value) {
		this.avg = value;
	}

	public Double getAvg() {
		return avg;
	}

	public void setCount(Long value) {
		this.count = value;
	}

	public Long getCount() {
		return count;
	}

	public long[] getHistogram() {
		return histogram;
	}

	/**
	 * Converts {@link #histogram} to flot.categories compatible JSON
	 * 
	 * @return
	 */
	public String getHistogramJson() {
		StringBuffer sb = new StringBuffer();
		sb.append("[ ");
		for (int i = 0; i < histogram.length; i++) {
			if (i > 0)
				sb.append(", ");
			sb.append("['").append(i / 2.0).append("', ").append(histogram[i]).append("]");
		}
		sb.append(" ]");
		return sb.toString();
	}

	/**
	 * Fill {@link #histogram} with data
	 * 
	 * @param hist
	 */
	public void makeHistogram(List<Object[]> hist) {
		for (Object[] h : hist) {

			// Determining JPA data types
			// System.err.println("Hist " + h[0].getClass() + " " +
			// h[1].getClass());

			int index = (int) (((Float) h[0]) * 2);
			long count = (Long) h[1];

			// System.err.println("Index for " + h[0] + " = " + index +
			// " count=" + count);

			histogram[index] = count;
		}
	}

	public void updateMin(Float updateMin) {
		if (updateMin == null)
			return;
		this.min = this.min == null || this.min.floatValue() > updateMin ? updateMin : this.min;
	}

	public void updateMax(Float updateMax) {
		if (updateMax == null)
			return;
		this.max = this.max == null || this.max.floatValue() < updateMax ? updateMax : this.max;
	}

	public void updateCountAndAvg(Long count, Double avg) {
		if (count == null || count == 0 || avg == null)
			return;
		this.avg = this.avg == null || this.count == null || this.count == 0 ? avg : (this.avg * this.count + avg * count) / (this.count + count);
		this.count = this.count==null ? count : this.count + count;
	}

	/**
	 * Return object array to be used for i18n
	 * 
	 * @return
	 */
	public Object[] getElStats() {
		return new Object[] { count, avg, min, max };
	}
}

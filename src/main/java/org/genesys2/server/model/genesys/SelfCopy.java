package org.genesys2.server.model.genesys;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class SelfCopy {
	private static Log LOG = LogFactory.getLog(SelfCopy.class);
	private static Map<Method, Method> gettersSetters = new HashMap<Method, Method>();

	static {
		Method[] allMethods = AccessionData.class.getMethods();
		for (Method getter : allMethods) {
			String name = getter.getName();
			boolean isGet = false;

			if (name.startsWith("get")) {
				isGet = true;
				name = name.substring(3);
			} else if (name.startsWith("is") && getter.getReturnType().equals(Boolean.class)) {
				isGet = true;
				name = name.substring(2);
			}

			if (isGet) {
				LOG.info("Using getter " + getter);
				try {
					Method setter = AccessionData.class.getMethod("set" + name, getter.getReturnType());
					gettersSetters.put(getter, setter);
					LOG.info("Using setter " + setter);
				} catch (NoSuchMethodException | SecurityException e) {
					LOG.error("No setter for " + name);
				}
			}
		}
	}

	public static void copy(AccessionData source, AccessionData target) {
		for (Method getter : gettersSetters.keySet()) {
			try {
				Object val = getter.invoke(source);
				gettersSetters.get(getter).invoke(target, val);
			} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
				LOG.error(e.getMessage(), e);
			}
		}
	}

}

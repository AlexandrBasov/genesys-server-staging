/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.model.impl;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;

import org.genesys2.server.model.BusinessModel;
import org.hibernate.annotations.Type;

/**
 * A {@link License} entity represents one of the possible licenses in use by an
 * {@link FaoInstitute}.
 *
 * @author mobreza
 */
@Entity
@Table(name = "license")
public class License extends BusinessModel {

	private static final long serialVersionUID = -1240680237067019307L;

	@Column(length = 200, nullable = false)
	private String name;

	@Lob
	@Type(type = "org.hibernate.type.TextType")
	@Column(nullable = false)
	private String text;

	private float openess;

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getText() {
		return text;
	}

	public void setText(final String text) {
		this.text = text;
	}

	public float getOpeness() {
		return openess;
	}

	public void setOpeness(final float openess) {
		this.openess = openess;
	}

}

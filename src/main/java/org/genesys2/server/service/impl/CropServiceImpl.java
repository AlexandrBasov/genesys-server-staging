/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.genesys2.server.model.genesys.Taxonomy2;
import org.genesys2.server.model.impl.Crop;
import org.genesys2.server.model.impl.CropRule;
import org.genesys2.server.model.impl.CropTaxonomy;
import org.genesys2.server.persistence.domain.CropRepository;
import org.genesys2.server.persistence.domain.CropRuleRepository;
import org.genesys2.server.persistence.domain.CropTaxonomyRepository;
import org.genesys2.server.persistence.domain.Taxonomy2Repository;
import org.genesys2.server.service.CropService;
import org.genesys2.server.service.HtmlSanitizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(readOnly = true)
public class CropServiceImpl implements CropService {

	private static final String CACHE_CROP_TAXONOMYCROPS = "hibernate.org.genesys2.server.model.impl.Crop.taxonomyCrops";

	public static final Log LOG = LogFactory.getLog(CropServiceImpl.class);

	@Autowired
	CropRepository cropRepository;

	@Autowired
	CropRuleRepository cropRuleRepository;

	@Autowired
	CropTaxonomyRepository cropTaxonomyRepository;

	@Autowired
	private HtmlSanitizer htmlSanitizer;

	@Autowired
	private Taxonomy2Repository taxonomy2Repository;

	@Override
	public Crop getCrop(String shortName) {
		return cropRepository.findByShortName(shortName);
	}

	@PreAuthorize("hasRole('ADMINISTRATOR')")
	@Override
	@Transactional
	public Crop delete(Crop crop) {
		cropRepository.delete(crop);
		crop.setId(null);
		return crop;
	}

	@Override
	public List<Crop> listCrops() {
		return cropRepository.findAll();
	}

	@Override
	public List<Crop> list(final Locale locale) {
		final List<Crop> crops = cropRepository.findAll();
		Collections.sort(crops, new Comparator<Crop>() {
			@Override
			public int compare(Crop o1, Crop o2) {
				return o1.getName(locale).compareTo(o2.getName(locale));
			}
		});
		return crops;
	}

	@Override
	@Cacheable(value = CACHE_CROP_TAXONOMYCROPS, key = "#taxonomy2.id")
	public List<Crop> getCrops(Taxonomy2 taxonomy2) {
		return cropTaxonomyRepository.findCropsByTaxonomy(taxonomy2);
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	@PreAuthorize("hasRole('ADMINISTRATOR')")
	@CacheEvict(value = CACHE_CROP_TAXONOMYCROPS, allEntries = true)
	public void rebuildTaxonomies() {
		LOG.warn("REBUILDING ALL CROP TAXONOMIES!");

		// for each crop
		for (final Crop crop : cropRepository.findAll()) {
			rebuildTaxonomies(crop);
		}

	}

	@Override
	@Transactional(readOnly = false)
	@PreAuthorize("hasRole('ADMINISTRATOR')")
	@CacheEvict(value = CACHE_CROP_TAXONOMYCROPS, allEntries = true)
	public void rebuildTaxonomies(Crop crop) {
		LOG.info("Rebuilding crop taxonomy for: " + crop.getName());
		// for all rules
		final List<CropRule> cropRules = crop.getCropRules();
		LOG.warn("Using rules: " + cropRules);
		rebuildCropTaxonomies(crop, cropRules);
	}

	private void rebuildCropTaxonomies(Crop crop, List<CropRule> cropRules) {

		final List<Taxonomy2> taxa = new ArrayList<Taxonomy2>();

		for (final CropRule cr : cropRules) {
			if (!cr.isIncluded())
				continue;

			if (cr.getSpecies() == null && cr.getSubtaxa() == null) {
				taxa.addAll(taxonomy2Repository.findByGenus(cr.getGenus()));
			} else if (cr.getSubtaxa() == null) {
				taxa.addAll(taxonomy2Repository.findByGenusAndSpecies(cr.getGenus(), cr.getSpecies()));
			} else {
				taxa.addAll(taxonomy2Repository.findByGenusAndSpeciesAndSubtaxa(cr.getGenus(), cr.getSpecies(), cr.getSubtaxa()));
			}
		}

		for (final CropRule cr : cropRules) {
			if (cr.isIncluded())
				continue;

			CollectionUtils.filterInverse(taxa, new Predicate<Taxonomy2>() {
				@Override
				public boolean evaluate(Taxonomy2 taxonomy) {
					if (cr.getSpecies() == null && cr.getSubtaxa() == null) {
						return StringUtils.equalsIgnoreCase(taxonomy.getSubtaxa(), cr.getSubtaxa())
								&& StringUtils.equalsIgnoreCase(taxonomy.getSpecies(), cr.getSpecies());
					} else if (cr.getSubtaxa() == null) {
						return StringUtils.equalsIgnoreCase(taxonomy.getSpecies(), cr.getSpecies());
					} else {
						return StringUtils.equalsIgnoreCase(taxonomy.getGenus(), cr.getGenus());
					}
				}
			});
		}

		// To check
		final List<CropTaxonomy> existing = cropTaxonomyRepository.findByCrop(crop);

		{
			final List<CropTaxonomy> toRemove = new ArrayList<CropTaxonomy>();

			// Remove existing if not in list
			for (final CropTaxonomy ct : existing) {
				final long taxonomyId = ct.getTaxonomy().getId();
				boolean found = false;

				for (final Taxonomy2 taxonomy : taxa) {
					if (taxonomyId == taxonomy.getId()) {
						found = true;
						break;
					}
				}
				if (!found) {
					toRemove.add(ct);
				}
			}
			cropTaxonomyRepository.delete(toRemove);
			LOG.info("Removed crop taxonomy for " + crop.getName() + " size=" + toRemove.size());
		}
		{
			final List<CropTaxonomy> toAdd = new ArrayList<CropTaxonomy>();

			// To add
			for (final Taxonomy2 taxonomy : taxa) {
				boolean found = false;

				for (final CropTaxonomy ct : existing) {
					if (ct.getTaxonomy().getId().equals(taxonomy.getId())) {
						found = true;
						break;
					}
				}
				if (!found) {
					final CropTaxonomy ct = new CropTaxonomy();
					ct.setCrop(crop);
					ct.setTaxonomy(taxonomy);
					toAdd.add(ct);
				}
			}

			cropTaxonomyRepository.save(toAdd);
			LOG.info("Added crop taxonomy for " + crop.getName() + " size=" + toAdd.size());
		}
	}

	@Override
	public Page<CropTaxonomy> getCropTaxonomies(Crop crop, Pageable pageable) {
		return cropTaxonomyRepository.findByCrop(crop, pageable);
	}

	/**
	 * Register a crop with Genesys
	 */
	@Override
	@PreAuthorize("hasRole('ADMINISTRATOR')")
	@Transactional(readOnly = false)
	public Crop addCrop(String shortName, String name, String description, String i18n) {
		LOG.info("Adding crop " + shortName);
		final Crop crop = new Crop();

		crop.setShortName(shortName);
		crop.setName(name);
		crop.setDescription(StringUtils.defaultIfBlank(htmlSanitizer.sanitize(description), null));
		crop.setI18n(StringUtils.defaultIfBlank(i18n, null));
		cropRepository.save(crop);
		LOG.info("Registered crop: " + crop);

		return crop;
	}

	/**
	 * Update a crop record in Genesys
	 */
	@Override
	@PreAuthorize("hasRole('ADMINISTRATOR') or hasPermission(#crop, 'ADMINISTRATION')")
	@Transactional(readOnly = false)
	public Crop updateCrop(Crop crop, String name, String description, String i18n) {
		LOG.info("Updating crop " + crop);

		if (crop != null) {

			name = StringUtils.defaultIfBlank(name, null);
			if (name != null) {
				crop.setName(name);
			}

			description = StringUtils.defaultIfBlank(htmlSanitizer.sanitize(description), null);
			if (description != null) {
				crop.setDescription(description);
			}

			i18n = StringUtils.defaultIfBlank(i18n, null);
			if (i18n != null) {
				crop.updateI18n(i18n);
			}

			cropRepository.save(crop);

			LOG.info("Updated crop: " + crop);
		}
		return crop;
	}

	@Override
	@PreAuthorize("hasRole('ADMINISTRATOR') or hasPermission(#crop, 'ADMINISTRATION')")
	@Transactional(readOnly = false)
	public CropRule addCropRule(Crop crop, String genus, String species, boolean included) {
		if (crop == null || StringUtils.isBlank(genus)) {
			LOG.debug("Ignoring blank rule");
			return null;
		}
		final CropRule rule = new CropRule();
		rule.setCrop(crop);
		rule.setGenus(genus);
		rule.setSpecies(StringUtils.defaultIfBlank(species, null));
		rule.setIncluded(included);
		cropRuleRepository.save(rule);
		return rule;
	}

	@Override
	@PreAuthorize("hasRole('ADMINISTRATOR') or hasPermission(#crop, 'ADMINISTRATION')")
	@Transactional(readOnly = false)
	public void setCropRules(Crop crop, List<CropRule> cropRules) {
		cropRuleRepository.deleteByCrop(crop);
		for (CropRule cr : cropRules) {
			cr.setId(null);
			cr.setCrop(crop);
		}
		cropRuleRepository.save(cropRules);
	}

	@Override
	public List<CropRule> getCropRules(Crop crop) {
		return crop == null ? null : cropRuleRepository.findByCrop(crop);
	}

	@Override
	@Transactional(readOnly = false)
	@CacheEvict(value = CACHE_CROP_TAXONOMYCROPS, key="#taxonomy.id")
	public void updateCropTaxonomyLists(Taxonomy2 taxonomy) {
		// Load all rules
		List<CropRule> cropRules = cropRuleRepository.findAll();

		Set<Crop> crops = new HashSet<Crop>();

		// Where does it fit
		for (CropRule cr : cropRules) {
			if (!cr.isIncluded())
				continue;

			if (cr.matches(taxonomy.getGenus(), taxonomy.getSpecies(), taxonomy.getSpAuthor(), taxonomy.getSubtaxa(), taxonomy.getSubtAuthor())) {
				crops.add(cr.getCrop());
			}
		}

		// Where doesn't it fit
		for (CropRule cr : cropRules) {
			if (cr.isIncluded())
				continue;

			// Skip crops we don't have
			if (!crops.contains(cr.getCrop()))
				continue;

			if (cr.matches(taxonomy.getGenus(), taxonomy.getSpecies(), taxonomy.getSpAuthor(), taxonomy.getSubtaxa(), taxonomy.getSubtAuthor())) {
				crops.remove(cr.getCrop());
			}
		}

		// Ensure we have matching entries in CropTaxonomy
		List<CropTaxonomy> existing = cropTaxonomyRepository.findByTaxonomy(taxonomy);

		{
			List<CropTaxonomy> toRemove = new ArrayList<CropTaxonomy>();
			for (CropTaxonomy ct : existing) {
				if (!crops.contains(ct.getCrop())) {
					toRemove.add(ct);
				}
			}
			cropTaxonomyRepository.delete(toRemove);
		}

		{
			List<CropTaxonomy> toAdd = new ArrayList<CropTaxonomy>();
			for (Crop crop : crops) {
				boolean found = false;
				for (CropTaxonomy ct : existing) {
					if (crop.equals(ct.getCrop())) {
						found = true;
					}
				}
				if (!found) {
					CropTaxonomy ct = new CropTaxonomy();
					ct.setCrop(crop);
					ct.setTaxonomy(taxonomy);
					toAdd.add(ct);
				}
			}
			cropTaxonomyRepository.save(toAdd);
		}
	}
}

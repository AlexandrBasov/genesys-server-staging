package org.genesys2.server.service.impl;

import java.io.IOException;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.genesys2.server.model.impl.Country;
import org.genesys2.server.model.impl.GeoRegion;
import org.genesys2.server.persistence.domain.CountryRepository;
import org.genesys2.server.persistence.domain.GeoRegionRepository;
import org.genesys2.server.service.GeoRegionService;
import org.genesys2.server.service.worker.GeoRegionDataCLDR;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.xml.sax.SAXException;

@Service
@Transactional(readOnly = true)
public class GeoRegionServiceImpl implements GeoRegionService {

	public static final Log LOG = LogFactory.getLog(GeoRegionServiceImpl.class);

	@Autowired
	CountryRepository countryRepository;

	@Autowired
	GeoRegionRepository geoRegionRepository;

	@Autowired
	GeoRegionDataCLDR geoRegionDataCLDR;

	@Override
	public GeoRegion find(String regionIsoCode) {
		return geoRegionRepository.findeByIsoCode(regionIsoCode);
	}

	@Override
	public void save(GeoRegion geoRegion) {
		geoRegionRepository.save(geoRegion);
	}

	@Override
	public void delete(GeoRegion geoRegion) {
		geoRegionRepository.delete(geoRegion);
	}

	@Override
	public GeoRegion getRegion(Country country) {
		return geoRegionRepository.findByCountry(country);
	}

	@Override
	public List<GeoRegion> findAll() {
		return geoRegionRepository.findAll();
	}

	@Override
	@Transactional(readOnly = false)
	public void updateGeoRegionData() throws IOException, ParserConfigurationException, SAXException {
		// update current geoRegions
		if (geoRegionDataCLDR == null) {
			LOG.warn("unicode.org geoRegions source not available");
			return;
		}
		List<GeoRegion> geoRegionList = geoRegionDataCLDR.getGeoRegionDataCLDR();

		if (null == geoRegionRepository.findAll() || geoRegionRepository.findAll().isEmpty()) {
			geoRegionRepository.save(geoRegionList);
			for (GeoRegion geoRegion : geoRegionList) {
				if (null != geoRegion.getCountries()) {
					for (Country country : geoRegion.getCountries()) {
						if (null != country) {
							country.setRegion(geoRegion);
						}
					}
				}
			}
		} else if (null != geoRegionRepository.findAll() || !geoRegionRepository.findAll().isEmpty()) {
			List<GeoRegion> list = geoRegionRepository.findAll();
			for (int i = 0; i < geoRegionList.size(); i++) {
				GeoRegion geoRegionForUpdate = list.get(i);
				geoRegionForUpdate.setName(geoRegionList.get(i).getName());
				geoRegionForUpdate.setIsoCode(geoRegionList.get(i).getIsoCode());
				geoRegionForUpdate.setNameL(geoRegionList.get(i).getNameL());
				geoRegionForUpdate.setCountries(geoRegionList.get(i).getCountries());

				geoRegionRepository.save(geoRegionForUpdate);

				if (null != geoRegionForUpdate.getCountries()) {
					for (Country country : geoRegionForUpdate.getCountries()) {
						if (null != country) {
							country.setRegion(geoRegionForUpdate);
							// countryRepository.save(country);
						}
					}
				}
			}
		}
	}
}

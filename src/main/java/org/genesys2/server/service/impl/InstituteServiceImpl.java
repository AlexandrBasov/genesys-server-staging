/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.collections4.ListUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.genesys2.server.model.impl.Country;
import org.genesys2.server.model.impl.FaoInstitute;
import org.genesys2.server.model.impl.FaoInstituteSetting;
import org.genesys2.server.persistence.domain.CountryRepository;
import org.genesys2.server.persistence.domain.FaoInstituteRepository;
import org.genesys2.server.persistence.domain.FaoInstituteSettingRepository;
import org.genesys2.server.persistence.domain.GenesysLowlevelRepository;
import org.genesys2.server.security.AuthUserDetails;
import org.genesys2.server.service.AclService;
import org.genesys2.server.service.ContentService;
import org.genesys2.server.service.InstituteService;
import org.genesys2.spring.SecurityContextUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.acls.domain.BasePermission;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(readOnly = true)
public class InstituteServiceImpl implements InstituteService {
	public static final Log LOG = LogFactory.getLog(InstituteServiceImpl.class);

	private static final List<FaoInstitute> EMPTY_LIST = ListUtils.unmodifiableList(new ArrayList<FaoInstitute>());

	@Autowired
	private FaoInstituteRepository instituteRepository;

	@Autowired
	private FaoInstituteSettingRepository instituteSettingRepository;

	@Autowired
	private CountryRepository countryRepository;

	@Autowired
	private ContentService contentService;

	@Autowired
    @Qualifier("genesysLowlevelRepositoryCustomImpl")
	private GenesysLowlevelRepository genesysLowlevelRepository;

	@Autowired
	private AclService aclService;

	@Override
	public Page<FaoInstitute> list(Pageable pageable) {
		return instituteRepository.listInstitutes(pageable);
	}

	@Override
	public Page<FaoInstitute> listActive(Pageable pageable) {
		return instituteRepository.listAllActive(pageable);
	}
	
	@Override
	public Page<FaoInstitute> listPGRInstitutes(Pageable pageable) {
		return instituteRepository.listPGRInstitutes(pageable);
	}

	@Override
	public long countActive() {
		return instituteRepository.countActive();
	}
	
	@Override
	public FaoInstitute getInstitute(String wiewsCode) {
		return instituteRepository.findByCode(wiewsCode);
	}

	@Override
	public FaoInstitute findInstitute(String wiewsCode) {
		final FaoInstitute inst = instituteRepository.findByCode(wiewsCode);
		if (inst != null) {
			inst.getSettings().size();
		}
		return inst;
	}

	@Override
	public List<FaoInstitute> listByCountry(Country country) {
		return instituteRepository.listByCountry(country, new Sort("code"));
	}

	@Override
	public List<FaoInstitute> listByCountryActive(Country country) {
		return instituteRepository.findByCountryActive(country, new Sort(Direction.DESC, "accessionCount", "code"));
	}

	@Override
	public List<FaoInstitute> getInstitutes(Collection<String> wiewsCodes) {
		if (wiewsCodes == null || wiewsCodes.size() == 0) {
			return EMPTY_LIST;
		}
		return instituteRepository.findAllByCodes(wiewsCodes);
	}

	@Override
	@Transactional(readOnly = false)
	@CacheEvict(value = "statistics", allEntries = true)
	public void update(Collection<FaoInstitute> institutes) {
		instituteRepository.save(institutes);
	}

	@Override
	@PreAuthorize("hasRole('ADMINISTRATOR') or hasPermission(#faoInstitute, 'ADMINISTRATION')")
	@Transactional(readOnly = false)
	public void updateAbout(FaoInstitute faoInstitute, String body, String summary, Locale locale) {
		contentService.updateArticle(faoInstitute, "blurp", null, body, summary, locale);
	}

	@Override
	@PreAuthorize("hasRole('ADMINISTRATOR') or hasPermission(#faoInstitute, 'ADMINISTRATION')")
	@Transactional(readOnly = false)
	public void setUniqueAcceNumbs(FaoInstitute faoInstitute, boolean uniqueAcceNumbs) {
		final FaoInstitute inst = instituteRepository.findOne(faoInstitute.getId());
		LOG.info("Setting 'uniqueAcceNumbs' to " + uniqueAcceNumbs + " for " + faoInstitute);
		inst.setUniqueAcceNumbs(uniqueAcceNumbs);
		instituteRepository.save(inst);
	}

	@Override
	@PreAuthorize("hasRole('ADMINISTRATOR') or hasPermission(#faoInstitute, 'ADMINISTRATION')")
	@Transactional(readOnly = false)
	public void setAllowMaterialRequests(FaoInstitute faoInstitute, boolean allowMaterialRequests) {
		final FaoInstitute inst = instituteRepository.findOne(faoInstitute.getId());
		LOG.info("Setting 'allowMaterialRequests' to " + allowMaterialRequests + " for " + faoInstitute);
		inst.setAllowMaterialRequests(allowMaterialRequests);
		instituteRepository.save(inst);
	}

	@Override
	@PreAuthorize("hasRole('ADMINISTRATOR') or hasPermission(#faoInstitute, 'ADMINISTRATION')")
	@Transactional(readOnly = false)
	public void setCodeSGSV(FaoInstitute faoInstitute, String codeSGSV) {
		final FaoInstitute inst = instituteRepository.findOne(faoInstitute.getId());
		LOG.info("Setting 'codeSGSV' to " + codeSGSV + " for " + faoInstitute);
		inst.setCodeSGSV(codeSGSV);
		instituteRepository.save(inst);
	}

	@Override
	@PreAuthorize("hasRole('ADMINISTRATOR') or hasPermission(#faoInstitute, 'ADMINISTRATION')")
	@Transactional(readOnly = false)
	public void updateSettings(FaoInstitute faoInstitute, Map<String, String> settings) {
		final List<FaoInstituteSetting> toSave = new ArrayList<FaoInstituteSetting>();
		final List<FaoInstituteSetting> toRemove = new ArrayList<FaoInstituteSetting>();

		for (final String key : settings.keySet()) {
			final String settingValue = StringUtils.defaultIfBlank(settings.get(key), null);

			FaoInstituteSetting setting = instituteSettingRepository.findByInstCodeAndSetting(faoInstitute.getCode(), key);
			if (setting == null && settingValue != null) {
				setting = new FaoInstituteSetting(faoInstitute);
				setting.setSetting(key);
				setting.setValue(settingValue);
				toSave.add(setting);
			} else if (setting != null) {
				setting.setValue(settingValue);
				if (settingValue == null) {
					toRemove.add(setting);
				} else {
					toSave.add(setting);
				}
			}
		}
		instituteSettingRepository.save(toSave);
	}

	@Override
	@PreAuthorize("hasRole('ADMINISTRATOR')")
	@Transactional(readOnly = false)
	@CacheEvict(value = "statistics", allEntries = true)
	public void updateCountryRefs() {
		genesysLowlevelRepository.updateFaoInstituteCountries();
	}

	@Override
	@Transactional
	@PreAuthorize("hasRole('ADMINISTRATOR')")
	@CacheEvict(value = "statistics", allEntries = true)
	public void delete(String instCode) {
		final FaoInstitute institute = getInstitute(instCode);
		if (institute != null) {
			instituteSettingRepository.deleteFor(institute.getCode());
			instituteRepository.delete(institute);
		}
	}

	@Override
	@PreAuthorize("isAuthenticated()")
	public List<FaoInstitute> listMyInstitutes(Sort sort) {
		final AuthUserDetails user = SecurityContextUtil.getAuthUser();
		final List<Long> oids = aclService.listIdentitiesForSid(FaoInstitute.class, user, BasePermission.WRITE);
		LOG.info("Got " + oids.size() + " elements for " + user.getUsername());
		if (oids.size() == 0) {
			return null;
		}

		return instituteRepository.findByIds(oids, sort);
	}

	@Override
	public List<FaoInstitute> autocomplete(String term) {
		return instituteRepository.autocomplete("%" + term + "%", new PageRequest(0, 10));
	}
}

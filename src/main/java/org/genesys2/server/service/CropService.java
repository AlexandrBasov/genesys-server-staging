/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.service;

import java.util.List;
import java.util.Locale;

import org.genesys2.server.model.genesys.Taxonomy2;
import org.genesys2.server.model.impl.Crop;
import org.genesys2.server.model.impl.CropRule;
import org.genesys2.server.model.impl.CropTaxonomy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface CropService {

	Crop getCrop(String shortName);

	List<Crop> listCrops();

	List<Crop> list(Locale locale);

	List<Crop> getCrops(Taxonomy2 taxonomy2);

	void rebuildTaxonomies();

	void rebuildTaxonomies(Crop crop);

	Page<CropTaxonomy> getCropTaxonomies(Crop crop, Pageable pageable);

	/**
	 * Add a {@link Crop} to the system
	 * 
	 * @param shortName
	 * @param name
	 * @param description
	 * @param i18n
	 * @return
	 */
	Crop addCrop(String shortName, String name, String description, String i18n);

	/**
	 * Updates a crop record
	 * 
	 * @param shortName
	 * @param name
	 * @param description
	 * @param i18n
	 * @return
	 */
	Crop updateCrop(Crop crop, String name, String description, String i18n);

	CropRule addCropRule(Crop crop, String genus, String species, boolean included);

	List<CropRule> getCropRules(Crop crop);

	/**
	 * Replace crop taxonomic rules
	 * 
	 * @param crop
	 * @param cropRules
	 */
	void setCropRules(Crop crop, List<CropRule> cropRules);

	/**
	 * Remove crop
	 * 
	 * @param crop
	 * @return
	 */
	Crop delete(Crop crop);

	/**
	 * Insert Taxonomy2 into correct CropTaxonomy lists
	 * 
	 * @param taxonomy
	 */
	void updateCropTaxonomyLists(Taxonomy2 taxonomy);

}

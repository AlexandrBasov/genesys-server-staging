/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.service.worker;

import static org.genesys2.util.NumberUtils.parseDoubleIgnore0;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.genesys2.server.model.impl.FaoInstitute;
import org.genesys2.server.service.GeoService;
import org.genesys2.server.service.InstituteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Component;

import au.com.bytecode.opencsv.CSVReader;

@Component
public class InstituteUpdater {
	public static final String WIEWS_EXPORT_URL = "http://www.fao.org/wiews-archive/export_c.zip";

	public static final Log LOG = LogFactory.getLog(InstituteUpdater.class);

	@Autowired
	private InstituteService instituteService;

	@Autowired
	private GeoService geoService;

	@Autowired
	private TaskExecutor taskExecutor;

	@Autowired
	private HttpClientBuilder httpClientBuilder;

	private static final int BATCH_SIZE = 50;

	/**
	 * Update local FaoInstitute with data from WIEWS database
	 *
	 * @throws IOException
	 */
	public void updateFaoInstitutes() throws IOException {

		final HttpGet httpget = new HttpGet(WIEWS_EXPORT_URL);

		HttpResponse response = null;

		final CloseableHttpClient httpclient = httpClientBuilder.build();
		try {
			response = httpclient.execute(httpget);

			LOG.debug(response.getStatusLine());

			// Get hold of the response entity
			final HttpEntity entity = response.getEntity();
			LOG.debug(entity.getContentType() + " " + entity.getContentLength());

			// If the response does not enclose an entity, there is no
			// need
			// to bother about connection release
			if (entity != null) {
				ZipInputStream instream = null;

				try {
					instream = new ZipInputStream(entity.getContent());
					final ZipEntry zipEntry = instream.getNextEntry();
					LOG.debug("Got entry: " + zipEntry.getName());
					if (!zipEntry.getName().equals("export_c.txt")) {
						LOG.warn("Expected export_c, not " + zipEntry.getName());
						throw new IOException("Missing export_c");
					}

					final InputStreamReader inreader = new InputStreamReader(instream, "UTF-8");
					final CSVReader reader = new CSVReader(inreader, ',', '"', false);

					final Map<String, String[]> batch = new HashMap<String, String[]>(BATCH_SIZE);

					String[] line = null;
					while ((line = reader.readNext()) != null) {
						for (int i = 0; i < line.length; i++) {
							if (line[i].equals("null") || StringUtils.isBlank(line[i])) {
								line[i] = null;
							}
						}
						// if (StringUtils.isNotBlank(line[14])) {
						// LOG.info(ArrayUtils.toString(line));
						// }

						final String instCode = line[0];
						batch.put(instCode, line);

						if (batch.size() == BATCH_SIZE) {
							workIt(batch);
							batch.clear();
						}

					}
					workIt(batch);
					batch.clear();

					reader.close();

				} catch (final RuntimeException ex) {
					LOG.error(ex.getMessage(), ex);
					httpget.abort();
				} finally {
					// Closing the input stream will trigger connection
					// release
					IOUtils.closeQuietly(instream);
				}
			}

			LOG.info("Done importing WIEWS database");
		} finally {
			IOUtils.closeQuietly(httpclient);
		}
	}

	private void workIt(final Map<String, String[]> batch) {

		// Need copy!
		final Map<String, String[]> batchCopy = new HashMap<String, String[]>(batch);

		taskExecutor.execute(new Runnable() {
			@Override
			public void run() {
				LOG.info("Processing next batch");

				// All instCodes in the batch
				final Collection<String> instCodes = batchCopy.keySet();

				// Fetch FaoInstitutes from DB
				final List<FaoInstitute> existing = instituteService.getInstitutes(instCodes);
				final List<FaoInstitute> toSave = new ArrayList<FaoInstitute>(instCodes.size());

				for (final FaoInstitute faoInstitute : existing) {

					final String[] line = batchCopy.get(faoInstitute.getCode());
					// Remove it
					batchCopy.remove(faoInstitute.getCode());

					if (updateData(faoInstitute, line)) {
						toSave.add(faoInstitute);
					}
				}

				for (final String instCode : batchCopy.keySet()) {
					LOG.info("Adding FaoInstitute with code: " + instCode);
					toSave.add(insertData(batchCopy.get(instCode)));
				}

				// Save what needs saving
				if (toSave.size() > 0) {
					LOG.info("Updating FaoInstitutes: " + toSave.size());
					instituteService.update(toSave);
				}

				// try {
				// Thread.sleep(2000);
				// } catch (final InterruptedException e) {
				// LOG.warn(e.getMessage());
				// }
			}

			private FaoInstitute insertData(String[] line) {
				final FaoInstitute faoInstitute = new FaoInstitute();
				updateData(faoInstitute, line);
				return faoInstitute;
			}

			private boolean updateData(FaoInstitute faoInstitute, String[] line) {
				final String instCode = line[0];
				final String acronym = line[1];
				// final String ecpaAcronym = line[2];
				final String fullName = line[3];
				final String type = line[4];
				final boolean pgrActivity = "Y".equals(line[5]);
				final boolean maintColl = "Y".equals(line[6]);
				// final String streetPob = line[7];
				// final String cityState = line[8];
				// final String zipCode = line[9];
				// final String phone = line[10];
				// final String fax = line[11];
				final String email = line[12];
				final String url = line[13];
				final String latitude = line[14];
				final String longitude = line[15];
				final String elevation = line[16];
				// final String updatedOn = line[17];
				// V_INSTCODE === New instcode?
				final String vInstCode = line[18];
				final String isoCountry = line[19];

				if (faoInstitute.getCode() == null) {
					faoInstitute.setCode(instCode);
				}
				faoInstitute.setAcronym(acronym);
				faoInstitute.setFullName(fullName);
				LOG.info("Updating: " + instCode + " " + fullName);
				faoInstitute.setPgrActivity(pgrActivity);
				faoInstitute.setMaintainsCollection(maintColl);
				faoInstitute.setEmail(email);
				faoInstitute.setType(type);
				faoInstitute.setUrl(url);
				final Double lat = parseDoubleIgnore0(latitude, 100);
				faoInstitute.setLatitude(lat);
				final Double lon = parseDoubleIgnore0(longitude, 100);
				faoInstitute.setLongitude(lon);
				final Double elev = parseDoubleIgnore0(elevation, 1);
				faoInstitute.setElevation(elev);
				faoInstitute.setvCode(vInstCode);
				faoInstitute.setCurrent(vInstCode == null || StringUtils.equals(instCode, vInstCode));

				// Update institute country if null or when not matching the
				// code
				faoInstitute.setCountry(geoService.getCurrentCountry(isoCountry));

				return true;
			}
		});
	}
}

package org.genesys2.server.service;

import org.genesys2.server.model.impl.Country;
import org.genesys2.server.model.impl.GeoRegion;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.List;

public interface GeoRegionService {

    GeoRegion find(String regionIsoCode);

    void save(GeoRegion geoRegion);

    void delete(GeoRegion geoRegion);

    List<GeoRegion> findAll();

    void updateGeoRegionData() throws IOException, ParserConfigurationException, SAXException;

	GeoRegion getRegion(Country country);

}

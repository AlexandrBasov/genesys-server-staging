/**
 * Copyright 2014 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.servlet.controller.rest.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.genesys2.server.model.kpi.Execution;

public class ExecutionJson {
	public Long id;
	public String name;
	public long version = 0;
	public String parameter;
	public String title;
	public Collection<ExecutionDimensionJson> dimensions;

	public static ExecutionJson from(Execution execution) {
		if (execution == null)
			return null;
		ExecutionJson ej = new ExecutionJson();
		ej.id = execution.getId();
		ej.name = execution.getName();
		ej.version = execution.getVersion();
		ej.parameter = execution.getParameter().getName();
		ej.title = execution.getTitle();
		ej.dimensions = ExecutionDimensionJson.from(execution.getExecutionDimensions());
		return ej;
	}

	public static Collection<ExecutionJson> from(List<Execution> executions) {
		if (executions == null) {
			return null;
		}
		List<ExecutionJson> list = new ArrayList<ExecutionJson>(executions.size());
		for (Execution e : executions) {
			list.add(from(e));
		}
		return list;
	}
}

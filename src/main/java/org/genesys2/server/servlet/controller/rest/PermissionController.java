/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.servlet.controller.rest;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.genesys2.server.model.acl.AclObjectIdentity;
import org.genesys2.server.model.impl.User;
import org.genesys2.server.service.AclService;
import org.genesys2.server.service.UserService;
import org.genesys2.server.servlet.model.PermissionJson;
import org.genesys2.server.servlet.util.PermissionJsonUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@PreAuthorize("isAuthenticated()")
@RequestMapping(value = { "/api/v0/permission", "/json/v0/permission" })
public class PermissionController extends RestController {
	private static final Log LOG = LogFactory.getLog(PermissionController.class);

	@Autowired
	protected AclService aclService;

	@Autowired
	private UserService userService;

	@RequestMapping(value = "/add", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody
	Object addPermission(@RequestBody PermissionJson permissionJson) {
		LOG.info("Adding permission " + permissionJson);
		final User user = userService.getUserByEmail(permissionJson.getUuid());

		if (user != null) {
			final Map<Integer, Boolean> permissionMap = PermissionJsonUtil.createPermissionsMap(permissionJson);
			aclService.addPermissions(permissionJson.getOid(), permissionJson.getClazz(), user.getUuid(), permissionJson.isPrincipal(), permissionMap);
			return JSON_OK;
		} else {
			throw new RuntimeException("No such user.");
		}
	}
	

	@RequestMapping(value = "/update", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody
	Object updatePermissions(@RequestBody PermissionJson permissionJson) {
		final Map<Integer, Boolean> permissionMap = PermissionJsonUtil.createPermissionsMap(permissionJson);

		final AclObjectIdentity objectIdentity = aclService.ensureObjectIdentity(permissionJson.getClazz(), permissionJson.getOid());
		aclService.updatePermission(objectIdentity, permissionJson.getUuid(), permissionMap);

		return JSON_OK;
	}


	@RequestMapping(value = "/autocompleteuser", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody
	List<String> acUser(@RequestParam("term") String email) {
		List<String> userEmails = new ArrayList<String>();
		for (User user : userService.autocompleteUser(email)) {
			userEmails.add(user.getEmail());
		}
		return userEmails;
	}
}

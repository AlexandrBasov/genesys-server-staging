/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.servlet.controller.transifex;

import java.io.IOException;
import java.util.Locale;

import org.genesys2.server.aspect.AsAdmin;
import org.genesys2.server.model.impl.Article;
import org.genesys2.server.service.ContentService;
import org.genesys2.server.servlet.controller.BaseController;
import org.genesys2.transifex.client.TransifexException;
import org.genesys2.transifex.client.TransifexService;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Transifex web hook listener.
 *
 * @author matijaobreza
 *
 */
@Controller
@RequestMapping(value = "/transifex")
public class TransifexAPIController extends BaseController {

	@Autowired
	private ContentService contentService;

	@Autowired(required = false)
	private TransifexService transifexService;

	@Value("${transifex.min.translated}")
	private int transifexMinTranslated;

	@Value("${transifex.hook.key}")
	private Object transifexHookKey;

	@Autowired
	private ObjectMapper mapper;

	/**
	 * Note: the hook key value should be set in preferences
	 */
	@AsAdmin
	@RequestMapping(value = "/hook/{hookKey:.+}", method = RequestMethod.POST)
	public @ResponseBody String webHookHandle(@PathVariable("hookKey") String hookKey, @RequestParam(value = "project") String projectSlug,
			@RequestParam(value = "resource") String resource, @RequestParam(value = "language") String language,
			@RequestParam(value = "translated") Integer translatedPercentage, Model model) {

		if (!transifexHookKey.equals(hookKey)) {
			_logger.error("Invalid key provided for Transifex callback hook: " + hookKey);
			throw new RuntimeException("I don't know you!");
		}

		if (!resource.startsWith("article-")) {
			_logger.warn("Ignoring Transifex'd hook for " + resource);
			return "Ignored";
		}

		String slug = resource.split("-")[1];
		_logger.warn("Transifex'd article slug=" + slug);

		if (translatedPercentage != null && translatedPercentage >= transifexMinTranslated) {
			// fetch updated resource from Transifex
			try {
				updateArticle(slug, language);
			} catch (TransifexException e) {
				_logger.error(e.getMessage(), e);
				throw new RuntimeException("Server error");
			}
			return "Thanks!";
		} else {
			return "Not sufficiently translated";
		}
	}

	private Article updateArticle(String slug, String lang) throws TransifexException {
		if (transifexService == null) {
			_logger.error("translationService not available.");
			throw new TransifexException("transifex.com service not available", null);
		}
		
		_logger.info("Fetching updated translation for article " + slug + " lang=" + lang);

		Locale locale = new Locale(lang);
		_logger.warn("Locale: " + locale);

		String resourceBody = transifexService.getTranslatedResource("article-".concat(slug), locale);
		String title = null;
		String body = null;
		String summary = null;
		
		try {
			JsonNode jsonObject = mapper.readTree(resourceBody);
			String content = jsonObject.get("content").asText();

			Document doc = Jsoup.parse(content);
			title = doc.title();
			_logger.info("Title: " + title);
			
			if (content.contains("class=\"summary")) {
				// 1st <div class="summary">...
				summary = doc.body().child(0).html();
				_logger.info("Summary: " + summary);

				// 2nd <div class="body">...
				body = doc.body().child(1).html();
				_logger.info("Body: " + body);
				
			} else {
				// Old fashioned body-only approach
				body = doc.body().html();
			}
		} catch (IOException e) {
			_logger.error(e.getMessage(), e);
			throw new TransifexException("Oops", e);
		}

		// Extract article from database we need (correct locale + do not use
		// default (EN) language)
		Article article = contentService.getGlobalArticle(slug, locale, false);

		// TODO Enable when tested
		// if (article == null) {
		// // No article for selected locale
		// article = contentService.createGlobalArticle(slug, locale, title,
		// body);
		// } else {
		// // Update article for locale
		// article = contentService.updateGlobalArticle(slug, locale, title,
		// body);
		// }

		_logger.info("Updated translation for article " + slug + " lang=" + lang);
		return article;
	}
}

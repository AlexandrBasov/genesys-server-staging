/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.servlet.controller;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.genesys2.server.exception.UserException;
import org.genesys2.server.model.impl.User;
import org.genesys2.server.service.ContentService;
import org.genesys2.server.service.EMailVerificationService;
import org.genesys2.server.service.TeamService;
import org.genesys2.server.service.TokenVerificationService.NoSuchVerificationTokenException;
import org.genesys2.server.service.UserService;
import org.genesys2.spring.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/profile")
public class UserProfileController extends BaseController {

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private UserService userService;

	@Autowired
	private Validator validator;

	@Autowired
	private TeamService teamService;

	@Autowired
	private ContentService contentService;

	@Autowired
	private EMailVerificationService emailVerificationService;

	@Value("${captcha.privateKey}")
	private String captchaPrivateKey;

	@Value("${captcha.publicKey}")
	private String captchaPublicKey;

	@RequestMapping
	@PreAuthorize("isAuthenticated()")
	public String welcome(ModelMap model) {
		final User user = userService.getMe();
		return "redirect:/profile/" + user.getUuid();
	}

	@RequestMapping("/list")
	@PreAuthorize("hasRole('ADMINISTRATOR')")
	public String list(ModelMap model, @RequestParam(value = "page", defaultValue = "1") int page) {
		model.addAttribute("pagedData", userService.listUsers(new PageRequest(page - 1, 50, new Sort("name"))));
		return "/user/index";
	}

	@RequestMapping("/{uuid:.+}/vetted-user")
	@PreAuthorize("hasRole('ADMINISTRATOR')")
	public String addRoleVettedUser(@PathVariable("uuid") String uuid) {
		userService.addVettedUserRole(uuid);
		return "redirect:/profile/" + uuid;
	}

	@RequestMapping("/{uuid:.+}")
	@PreAuthorize("isAuthenticated()")
	public String someProfile(ModelMap model, @PathVariable("uuid") String uuid) {
		final User user = userService.getUserByUuid(uuid);
		if (user == null) {
			throw new ResourceNotFoundException();
		}

		model.addAttribute("user", user);
		model.addAttribute("teams", teamService.listUserTeams(user));

		return "/user/profile";
	}

	@RequestMapping("/{uuid:.+}/edit")
	@PreAuthorize("hasRole('ADMINISTRATOR') || principal.user.uuid == #uuid")
	public String edit(ModelMap model, @PathVariable("uuid") String uuid) {
		someProfile(model, uuid);
		model.addAttribute("availableRoles", userService.listAvailableRoles());
		return "/user/edit";
	}

	@RequestMapping(value = "/{uuid}/send", method = RequestMethod.GET)
	@PreAuthorize("hasRole('ADMINISTRATOR') || principal.user.uuid == #uuid")
	public String sendEmail(ModelMap model, @PathVariable("uuid") String uuid) {

		final User user = userService.getUserByUuid(uuid);
		emailVerificationService.sendVerificationEmail(user);

		return "redirect:/profile/" + user.getUuid();
	}

	@RequestMapping(value = "/{tokenUuid:.+}/cancel", method = RequestMethod.GET)
	public String cancelValidation(ModelMap model, @PathVariable("tokenUuid") String tokenUuid) {
		emailVerificationService.cancelValidation(tokenUuid);
		return "redirect:/";
	}

	@RequestMapping(value = "/{tokenUuid:.+}/validate", method = RequestMethod.GET)
	public String validateEmail(ModelMap model, @PathVariable("tokenUuid") String tokenUuid) {
		model.addAttribute("tokenUuid", tokenUuid);
		return "/user/validateemail";
	}

	@RequestMapping(value = "/{tokenUuid:.+}/validate", method = RequestMethod.POST)
	public String validateEmail2(ModelMap model, @PathVariable("tokenUuid") String tokenUuid, @RequestParam(value = "key", required = true) String key) {
		try {
			emailVerificationService.validateEMail(tokenUuid, key);
			return "redirect:/profile";
		} catch (final NoSuchVerificationTokenException e) {
			// Not valid
			model.addAttribute("tokenUuid", tokenUuid);
			model.addAttribute("error", "error");
			return "/user/validateemail";
		}
	}

	@RequestMapping(value = "/password/reset", method = RequestMethod.POST)
	public String resetPassword(ModelMap model, @RequestParam("email") String email) {
		final User user = userService.getUserByEmail(email);

		if (user != null) {
			emailVerificationService.sendPasswordResetEmail(user);
		}

		return "redirect:/content/user.password-reset-email-sent";
	}

	@RequestMapping(value = "/{tokenUuid:.+}/pwdreset", method = RequestMethod.GET)
	public String passwordReset(ModelMap model, @PathVariable("tokenUuid") String tokenUuid) {
		model.addAttribute("tokenUuid", tokenUuid);
		return "/user/password";
	}

	@RequestMapping(value = "/{tokenUuid:.+}/pwdreset", method = RequestMethod.POST)
	public String updatePassword(ModelMap model, @PathVariable("tokenUuid") String tokenUuid, @RequestParam(value = "key", required = true) String key,
			@RequestParam("password") String password) throws UserException {

		try {
			emailVerificationService.changePassword(tokenUuid, key, password);
			return "redirect:/content/user.password-reset";
		} catch (final NoSuchVerificationTokenException e) {
			// Not valid
			model.addAttribute("tokenUuid", tokenUuid);
			model.addAttribute("error", "error");
			return "/user/password";
		}
	}

	@RequestMapping(value = "/{uuid:.+}/update", method = { RequestMethod.POST })
	@PreAuthorize("hasRole('ADMINISTRATOR') || principal.user.uuid == #uuid")
	public String update(ModelMap model, @PathVariable("uuid") String uuid, @RequestParam("name") String name, @RequestParam("email") String email,
			@RequestParam("pwd1") String pwd1, @RequestParam("pwd2") String pwd2) {
		final User user = userService.getUserByUuid(uuid);
		if (user == null) {
			throw new ResourceNotFoundException();
		}

		userService.updateData(user.getId(), name, email);

		if (StringUtils.isNotBlank(pwd1)) {
			if (pwd1.equals(pwd2)) {
				try {
					_logger.info("Updating password for " + user);
					userService.updatePassword(user.getId(), pwd1);
					_logger.warn("Password updated for " + user);
				} catch (final UserException e) {
					_logger.error(e.getMessage(), e);
				}
			} else {
				_logger.warn("Passwords didn't match for " + user);
			}
		}

		return "redirect:/profile/" + user.getUuid();
	}

	@RequestMapping(value = "/{uuid:.+}/update-roles", method = { RequestMethod.POST })
	@PreAuthorize("hasRole('ADMINISTRATOR')")
	public String updateRoles(ModelMap model, @PathVariable("uuid") String uuid, @RequestParam("role") List<String> selectedRoles) {
		final User user = userService.getUserByUuid(uuid);
		if (user == null) {
			throw new ResourceNotFoundException();
		}

		userService.updateRoles(user, selectedRoles);
		return "redirect:/profile/" + user.getUuid();
	}

}

/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.servlet.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.genesys2.server.model.impl.Article;
import org.genesys2.server.service.ContentService;
import org.genesys2.spring.RequestAttributeLocaleResolver;
import org.genesys2.spring.ResourceNotFoundException;
import org.genesys2.transifex.client.TransifexException;
import org.genesys2.transifex.client.TransifexService;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

@Controller
@RequestMapping("/content")
public class ArticleController extends BaseController {

	@Autowired
	private ContentService contentService;

	@Autowired(required = false)
	private TransifexService transifexService;

	@Autowired
	private RequestAttributeLocaleResolver localeResolver;

	@Resource
	private Set<String> supportedLocales;

	private static final String defaultLanguage = "en";

	@Autowired
	private ObjectMapper mapper;

	@RequestMapping(value = "/transifex", params = { "post" }, method = RequestMethod.POST)
	@PreAuthorize("hasRole('ADMINISTRATOR') or hasRole('CONTENTMANAGER')")
	public String postToTransifex(@RequestParam("slug") String slug, RedirectAttributes redirectAttrs) {
		if (transifexService == null) {
			throw new ResourceNotFoundException("translationService not enabled");
		}
		Article article = contentService.getGlobalArticle(slug, getLocale());
		String resourceName = "article-" + slug;
		String body = String.format("<div class=\"summary\">%s</div><div class=\"body\">%s</div>", article.getSummary(), article.getBody());

		try {
			if (transifexService.resourceExists(resourceName)) {
				transifexService.updateXhtmlResource(resourceName, article.getTitle(), body);
			} else {
				transifexService.createXhtmlResource(resourceName, article.getTitle(), body);
			}
		} catch (IOException e) {
			redirectAttrs.addFlashAttribute("responseFromTransifex", "article.transifex-failed");
			e.printStackTrace();
		}
		redirectAttrs.addFlashAttribute("responseFromTransifex", "article.transifex-resource-updated");

		return "redirect:/content/" + slug + "/edit/" + LocaleContextHolder.getLocale().getLanguage();
	}

	@RequestMapping(value = "/transifex", params = { "remove" }, method = RequestMethod.POST)
	@PreAuthorize("hasRole('ADMINISTRATOR') or hasRole('CONTENTMANAGER')")
	public String deleteFromTransifex(@RequestParam("slug") String slug, RedirectAttributes redirectAttrs) {
		if (transifexService == null) {
			throw new ResourceNotFoundException("translationService not enabled");
		}

		try {
			if (transifexService.deleteResource("article-" + slug)) {
				redirectAttrs.addFlashAttribute("responseFromTransifex", "article.transifex-resource-removed");
			} else {
				redirectAttrs.addFlashAttribute("responseFromTransifex", "article.transifex-failed");
			}
		} catch (TransifexException e) {
			_logger.error(e.getMessage(), e);
			redirectAttrs.addFlashAttribute("responseFromTransifex", "article.transifex-failed");
		}

		return "redirect:/content/" + slug + "/edit/" + LocaleContextHolder.getLocale().getLanguage();
	}

	/**
	 * Fetch all from Transifex and store
	 * 
	 * @param slug
	 * @param language
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/transifex", params = { "fetch-all" }, method = RequestMethod.POST)
	@PreAuthorize("hasRole('ADMINISTRATOR') or hasRole('CONTENTMANAGER')")
	public String fetchAllFromTransifex(@RequestParam("slug") String slug, RedirectAttributes redirectAttrs) throws Exception {
		if (transifexService == null) {
			throw new ResourceNotFoundException("translationService not enabled");
		}

		List<String> responses = new ArrayList<String>(20);

		for (String lang : supportedLocales) {
			if (defaultLanguage.equalsIgnoreCase(lang)) {
				continue;
			}

			Locale locale = Locale.forLanguageTag(lang);
			_logger.info("Fetching article " + slug + " translation for " + locale);

			String translatedResource;
			try {
				translatedResource = transifexService.getTranslatedResource("article-" + slug, locale);
			} catch (TransifexException e) {
				_logger.warn(e.getMessage(), e);
				if (e.getCause() != null) {
					responses.add(e.getLocalizedMessage() + ": " + e.getCause().getLocalizedMessage());
				} else {
					responses.add(e.getLocalizedMessage());
				}
				// throw new Exception(e.getMessage(), e);
				continue;
			}

			String title = null;
			String body = null;
			String summary = null;

			try {
				JsonNode jsonObject = mapper.readTree(translatedResource);
				String content = jsonObject.get("content").asText();

				Document doc = Jsoup.parse(content);
				title = doc.title();
				if (content.contains("class=\"summary")) {
					// 1st <div class="summary">...
					summary = doc.body().child(0).html();
					// 2nd <div class="body">...
					body = doc.body().child(1).html();
				} else {
					// Old fashioned body-only approach
					body = doc.body().html();
				}

				contentService.updateGlobalArticle(slug, locale, title, body, summary);
				responses.add("article.translations-updated");

			} catch (IOException e) {
				_logger.warn(e.getMessage(), e);
				responses.add(e.getMessage());
				continue;
			}
		}

		redirectAttrs.addFlashAttribute("responseFromTransifex", responses);
		return "redirect:/content/" + slug;
	}

	@RequestMapping(value = "/translate/{slug}/{language}", method = RequestMethod.GET)
	@PreAuthorize("hasRole('ADMINISTRATOR') or hasRole('CONTENTMANAGER')")
	public String fetchFromTransifex(@PathVariable("slug") String slug, @PathVariable("language") String language, Model model) throws Exception {
		if (transifexService == null) {
			throw new ResourceNotFoundException("translationService not enabled");
		}

		Locale locale = new Locale(language);

		Article article = contentService.getGlobalArticle(slug, locale, false);

		String translatedResource;
		try {
			translatedResource = transifexService.getTranslatedResource("article-" + slug, locale);
		} catch (TransifexException e) {
			_logger.error(e.getMessage(), e);
			throw new Exception(e.getMessage(), e);
		}

		String title = null;
		String body = null;
		String summary = null;

		try {
			JsonNode jsonObject = mapper.readTree(translatedResource);
			String content = jsonObject.get("content").asText();

			Document doc = Jsoup.parse(content);
			title = doc.title();
			if (content.contains("class=\"summary")) {
				// 1st <div class="summary">...
				summary = doc.body().child(0).html();
				// 2nd <div class="body">...
				body = doc.body().child(1).html();
			} else {
				// Old fashioned body-only approach
				body = doc.body().html();
			}
		} catch (IOException e) {
			_logger.error(e.getMessage(), e);
			throw e;
		}

		if (article == null) {
			article = new Article();
			article.setSlug(slug);
		}

		article.setTitle(title);
		article.setBody(body);
		article.setSummary(summary);
		article.setLang(language);

		model.addAttribute("article", article);
		return "/content/article-edit";
	}

	@RequestMapping
	@PreAuthorize("hasRole('ADMINISTRATOR') or hasRole('CONTENTMANAGER')")
	public String list(ModelMap model, @RequestParam(value = "page", defaultValue = "1") int page,
			@RequestParam(value = "language", defaultValue = "") String lang) {

		if (!lang.isEmpty()) {
			model.addAttribute("pagedData", contentService.listArticlesByLang(lang, new PageRequest(page - 1, 50, new Sort("slug"))));
		} else {
			model.addAttribute("pagedData", contentService.listArticles(new PageRequest(page - 1, 50, new Sort("slug"))));
		}

		// todo full name of locales
		Map<String, String> locales = new TreeMap<>();

		for (String language : localeResolver.getSupportedLocales()) {
			locales.put(new Locale(language).getDisplayName(), language);
		}

		model.addAttribute("languages", locales);
		model.addAttribute("language", lang);

		return "/content/index";
	}

	@RequestMapping("{url:.+}")
	public String view(ModelMap model, @PathVariable(value = "url") String slug) {
		_logger.debug("Viewing article " + slug);

		final Article article = contentService.getGlobalArticle(slug, getLocale());
		if (article == null) {
			if (hasRole("ADMINISTRATOR")) {
				return "redirect:/content/" + slug + "/edit";
			}
			throw new ResourceNotFoundException();
		}
		model.addAttribute("title", article.getTitle());
		model.addAttribute("article", article);

		return "/content/article";
	}

	@RequestMapping("{menu}/{url:.+}")
	public String viewWithMenu(ModelMap model, @PathVariable(value = "menu") String menuKey, @PathVariable(value = "url") String slug) {
		String result = view(model, slug);
		if (StringUtils.isNotBlank(menuKey)) {
			_logger.debug("Loading menu " + menuKey);
			model.addAttribute("menu", contentService.getMenu(menuKey));
		}
		return result;
	}

	/**
	 * Edit article in current language
	 * 
	 * @param model
	 * @param slug
	 * @return
	 */
	@PreAuthorize("hasRole('ADMINISTRATOR') or hasRole('CONTENTMANAGER')")
	@RequestMapping("{url:.+}/edit")
	public String edit(ModelMap model, @PathVariable(value = "url") String slug) {
		_logger.debug("Editing article " + slug);
		return "redirect:/content/" + slug + "/edit/" + LocaleContextHolder.getLocale().getLanguage();
	}

	/**
	 * Edit article in another language
	 * 
	 * @param model
	 * @param slug
	 * @param language
	 * @return
	 */
	@PreAuthorize("hasRole('ADMINISTRATOR') or hasRole('CONTENTMANAGER')")
	@RequestMapping("{url:.+}/edit/{language}")
	public String edit(ModelMap model, @PathVariable(value = "url") String slug, @PathVariable("language") String language) {
		_logger.debug("Editing article " + slug);

		Article article = contentService.getArticleBySlugAndLang(slug, language);
		if (article == null) {
			article = new Article();
			article.setSlug(slug);
			article.setLang(language);
		}
		model.addAttribute("article", article);

		return "/content/article-edit";
	}

	@PreAuthorize("hasRole('ADMINISTRATOR') or hasRole('CONTENTMANAGER')")
	@RequestMapping(value = "/save-article/{language}", method = { RequestMethod.POST })
	public String createNewGlobalArticle(ModelMap model, @RequestParam("slug") String slug, @PathVariable("language") String language,
			@RequestParam("title") String title, @RequestParam("body") String body, @RequestParam(value = "summary", required = false) String summary) {

		contentService.updateGlobalArticle(slug, new Locale(language), title, body, summary);

		return redirectAfterSave(slug, language);
	}

	private String redirectAfterSave(String slug, String language) {
		if (LocaleContextHolder.getLocale().getLanguage().equals(language)) {
			return "redirect:/content/" + slug;
		} else {
			return "redirect:/content/?language=" + language;
		}
	}

	@PreAuthorize("hasRole('ADMINISTRATOR') or hasRole('CONTENTMANAGER')")
	@RequestMapping(value = "/save-article/{language}", params = { "id" }, method = { RequestMethod.POST })
	public String saveExistingGlobalArticle(ModelMap model, @PathVariable("language") String language, @RequestParam("id") long id,
			@RequestParam("slug") String slug, @RequestParam("title") String title, @RequestParam("body") String body,
			@RequestParam(value = "summary", required = false) String summary) {

		Article article = contentService.updateArticle(id, slug, title, body, summary);

		return redirectAfterSave(slug, article.getLang());
	}

	@RequestMapping(value = "/blurp/update-blurp", method = { RequestMethod.POST })
	public String updateBlurp(ModelMap model, @RequestParam("id") long id, @RequestParam(required = false, value = "title") String title,
			@RequestParam("body") String body, @RequestParam(value = "summary", required = false) String summary) {

		contentService.updateArticle(id, null, title, body, summary);

		return "redirect:/";
	}

	@RequestMapping(value = "/blurp/create-blurp", method = { RequestMethod.POST })
	public String createBlurp(ModelMap model, @RequestParam("clazz") String clazz, @RequestParam("entityId") long entityId,
			@RequestParam(required = false, value = "title") String title, @RequestParam("body") String body,
			@RequestParam(value = "summary", required = false) String summary) throws ClassNotFoundException {

		contentService.updateArticle(Class.forName(clazz), entityId, "blurp", title, body, summary, getLocale());
		return "redirect:/";
	}
}

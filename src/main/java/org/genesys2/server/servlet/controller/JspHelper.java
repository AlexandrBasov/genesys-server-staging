/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.servlet.controller;

import java.text.DateFormatSymbols;
import java.util.Locale;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.RandomUtils;
import org.genesys2.server.exception.UserException;
import org.genesys2.server.model.impl.Country;
import org.genesys2.server.model.impl.Crop;
import org.genesys2.server.model.impl.User;
import org.genesys2.server.service.CropService;
import org.genesys2.server.service.GeoService;
import org.genesys2.server.service.HtmlConverter;
import org.genesys2.server.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class JspHelper {
	@Autowired
	private UserService userService;
	@Autowired
	private GeoService geoService;
	@Autowired
	private CropService cropService;
	@Autowired
	private ObjectMapper objectMapper;
	@Autowired
	private HtmlConverter htmlConverter;

	public String userFullName(Long userId) {
		if (userId == null) {
			return null;
		}
		try {
			final User user = userService.getUserById(userId);
			return user.getName();
		} catch (final UserException e) {
			return null;
		}
	}

	public User userByUuid(String uuid) {
		if (uuid == null) {
			return null;
		}
		return userService.getUserByUuid(uuid);
	}

	public Country getCountry(String iso3) {
		return geoService.getCountry(iso3);
	}

	public Crop getCrop(String shortName) {
		return cropService.getCrop(shortName);
	}

	public String toJson(Object object) throws JsonProcessingException {
		return objectMapper.writer().writeValueAsString(object);
	}

	public String htmlToText(String html) {
		return htmlConverter.toText(html);
	}

	public String htmlToText(String html, int maxLength) {
		return StringUtils.abbreviate(htmlConverter.toText(html), maxLength);
	}

	public String abbreviate(String text, int maxLength) {
		return StringUtils.abbreviate(text, maxLength);
	}

	public String[] monthNames(Locale locale) {
		DateFormatSymbols dfs = new DateFormatSymbols(locale);
		return dfs.getMonths();
	}

	public String[] monthShortNames(Locale locale) {
		DateFormatSymbols dfs = new DateFormatSymbols(locale);
		return dfs.getShortMonths();
	}

	public int randomInt(int min, int max) {
		return min + RandomUtils.nextInt(max - min + 1);
	}
}

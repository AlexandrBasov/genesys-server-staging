/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.servlet.controller;

import java.util.Collection;

import org.genesys2.server.model.oauth.OAuthAccessToken;
import org.genesys2.server.model.oauth.OAuthClientDetails;
import org.genesys2.server.model.oauth.OAuthClientType;
import org.genesys2.server.model.oauth.OAuthRefreshToken;
import org.genesys2.server.service.JPATokenStore;
import org.genesys2.server.service.OAuth2ClientDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/management")
public class OAuthManagementController extends BaseController {

	@Autowired
	private OAuth2ClientDetailsService clientDetailsService;

	@Autowired
	@Qualifier("tokenStore")
	private JPATokenStore tokenStore;

	@PreAuthorize("hasRole('ADMINISTRATOR')")
	@RequestMapping("/")
	public String listClients(Model model) {
		model.addAttribute("clientDetailsList", clientDetailsService.listClientDetails());
		return "/oauth/clientslist";
	}

	@PreAuthorize("hasRole('ADMINISTRATOR')")
	@RequestMapping("/{clientId}/removeall")
	public String removeAllAccessTokens(@PathVariable("clientId") String clientId) {

		final Collection<OAuth2AccessToken> tokens = tokenStore.findTokensByClientId(clientId);
		for (final OAuth2AccessToken token : tokens) {
			tokenStore.removeAccessToken(token);
		}

		return "redirect:/management/" + clientId + "/";
	}

	@PreAuthorize("hasRole('ADMINISTRATOR')")
	@RequestMapping("/{clientId}/removeall-rt")
	public String removeAllRefreshTokens(@PathVariable("clientId") String clientId) {

		final Collection<OAuthRefreshToken> tokens = tokenStore.findRefreshTokensByClientId(clientId);
		for (final OAuthRefreshToken token : tokens) {
			tokenStore.removeRefreshToken(token.getId());
		}

		return "redirect:/management/" + clientId + "/";
	}

	@PreAuthorize("hasRole('ADMINISTRATOR')")
	@RequestMapping("/{clientId}/{tokenId}/remove-rt")
	public String removeRefreshToken(@PathVariable("tokenId") long tokenId, @PathVariable("clientId") String clientId) {

		tokenStore.removeRefreshToken(tokenId);
		return "redirect:/management/" + clientId + "/";
	}

	@PreAuthorize("hasRole('ADMINISTRATOR')")
	@RequestMapping("/{clientId}/{tokenId}/remove")
	public String removeAccessToken(@PathVariable("tokenId") long tokenId, @PathVariable("clientId") String clientId) {

		tokenStore.removeAccessToken(tokenId);
		return "redirect:/management/" + clientId + "/";
	}

	@RequestMapping("/user/{uuid}/tokens")
	@PreAuthorize("hasRole('ADMINISTRATOR') || principal.user.uuid == #uuid")
	public String getIssuedTokens(@PathVariable("uuid") String uuid, Model model) {
		final Collection<OAuthAccessToken> tokens = clientDetailsService.findTokensByUserUuid(uuid);
		model.addAttribute("tokens", tokens);
		return "/oauth/tokenslist";
	}

	@RequestMapping("/user/{uuid}/{tokenId}/remove")
	@PreAuthorize("hasRole('ADMINISTRATOR') || principal.user.uuid == #uuid")
	public String removeUsersAccessToken(@PathVariable("tokenId") long tokenId, @PathVariable("uuid") String uuid) {
		tokenStore.removeAccessToken(tokenId);
		return "redirect:/management/user/" + uuid + "/tokens";
	}

	@PreAuthorize("hasAnyRole('VETTEDUSER','ADMINISTRATOR')")
	@RequestMapping("/add-client")
	public String addClientEntry() {
		return "/oauth/edit";
	}

	@PreAuthorize("hasAnyRole('VETTEDUSER','ADMINISTRATOR')")
	@RequestMapping("/{id}/edit")
	public String editClient(Model model, @PathVariable("id") long id) {
		final ClientDetails clientDetails = clientDetailsService.getClientDetails(id);
		model.addAttribute("clientDetails", clientDetails);
		return "/oauth/edit";
	}

	@PreAuthorize("hasAnyRole('VETTEDUSER','ADMINISTRATOR')")
	@RequestMapping(value = "/save-client", method = RequestMethod.POST, params = "newClient=new")
	public String createClientEntry(Model model, @RequestParam("title") String title, @RequestParam("description") String description,
			@RequestParam(value = "redirectUris", required = false) String redirectUris,
			@RequestParam("accessTokenValiditySeconds") Integer accessTokenValiditySeconds,
			@RequestParam("refreshTokenValiditySeconds") Integer refreshTokenValiditySeconds,
			@RequestParam(value = "clientType", required = false) OAuthClientType clientType) {

		OAuthClientDetails clientDetails = clientDetailsService.addClientDetails(title, description, redirectUris, accessTokenValiditySeconds,
				refreshTokenValiditySeconds, clientType);

		return "redirect:/management/" + clientDetails.getId() + "/edit";
	}

	@PreAuthorize("hasAnyRole('VETTEDUSER','ADMINISTRATOR')")
	@RequestMapping(value = "/save-client", method = RequestMethod.POST, params = { "id", "action-delete" })
	public String deleteClient(Model model, @RequestParam("id") long id) {
		final OAuthClientDetails clientDetails = clientDetailsService.getClientDetails(id);
		_logger.info("Deleting client " + clientDetails.getClientId());
		clientDetailsService.removeClient(clientDetails);
		return "redirect:/management/";
	}

	@RequestMapping(value = "/save-client", method = RequestMethod.POST, params = { "id", "action-save" })
	public String saveExistinClient(Model model, @RequestParam("title") String title, @RequestParam("description") String description,
			@RequestParam("id") long id, @RequestParam(value = "client_secret", required = false) String clientSecret,
			@RequestParam(value = "redirectUris", required = false) String redirectUris,
			@RequestParam("accessTokenValiditySeconds") Integer accessTokenValiditySeconds,
			@RequestParam("refreshTokenValiditySeconds") Integer refreshTokenValiditySeconds,
			@RequestParam(value = "clientType", required = false) OAuthClientType clientType) {

		final OAuthClientDetails clientDetails = clientDetailsService.update(clientDetailsService.getClientDetails(id), title, description, clientSecret, redirectUris,
				accessTokenValiditySeconds, refreshTokenValiditySeconds);

		return "redirect:/management/" + clientDetails.getId() + "/edit";
	}

	@PreAuthorize("hasRole('ADMINISTRATOR')")
	@RequestMapping(value = "/{clientId}", method = RequestMethod.GET)
	public String clientDetailsInfo(Model model, @PathVariable("clientId") String clientId) {
		final ClientDetails clientDetails = clientDetailsService.loadClientByClientId(clientId);
		final Collection<OAuthAccessToken> tokensByClientId = clientDetailsService.findTokensByClientId(clientId);
		final Collection<OAuthRefreshToken> refreshTokensByClientId = clientDetailsService.findRefreshTokensClientId(clientId);

		model.addAttribute("accessTokens", tokensByClientId);
		model.addAttribute("refreshTokens", refreshTokensByClientId);
		model.addAttribute("clientDetails", clientDetails);
		return "/oauth/detailsinfo";
	}

}

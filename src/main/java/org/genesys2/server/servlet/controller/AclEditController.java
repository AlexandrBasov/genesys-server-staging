/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.servlet.controller;

import org.genesys2.server.model.acl.AclObjectIdentity;
import org.genesys2.server.service.AclService;
import org.genesys2.server.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@Scope("request")
@RequestMapping("/acl")
@PreAuthorize("isAuthenticated()")
public class AclEditController extends BaseController {

	@Autowired
	private AclService aclService;

	@Autowired
	private UserService userService;

	@RequestMapping("/{clazz}/{id}/permissions")
	public String permissions(ModelMap model, @PathVariable(value = "clazz") String className, @PathVariable("id") long id,
			@RequestParam(value = "back", required = false) String backUrl) {

		final AclObjectIdentity objectIdentity = aclService.ensureObjectIdentity(className, id);
		model.addAttribute("aclObjectIdentity", objectIdentity);
		if (objectIdentity != null) {
			model.addAttribute("aclPermissions", aclService.getAvailablePermissions(className));
		}
		model.addAttribute("aclSids", aclService.getSids(id, className));
		// Map<AclSid, Map<Permission, Boolean>>
		model.addAttribute("aclEntries", aclService.getPermissions(id, className));
		model.addAttribute("backUrl", backUrl);

		return "/acl/editor";
	}

}

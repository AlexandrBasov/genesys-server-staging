/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.servlet.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.social.google.api.plus.Person;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class GoogleOAuthUtil {
	private static final Log LOG = LogFactory.getLog(GoogleOAuthUtil.class);
	public static final String LOCAL_GOOGLEAUTH_PATH = "/google/auth";

	private ObjectMapper mapper = new ObjectMapper();

	@Value("${base.url}")
	private String baseUrl;

	@Value("${google.consumerKey}")
	private String googleApiClientId;

	@Value("${google.consumerSecret}")
	private String secret;

	@Autowired
	@Named("authUserDetailsService")
	private UserDetailsService userDetailsService;

	public String exchangeForAccessToken(HttpServletRequest request) throws IOException {
		final CloseableHttpClient httpclient = HttpClientBuilder.create().build();
		
		try {
			final HttpPost httppost = new HttpPost("https://accounts.google.com/o/oauth2/token");

			final List<NameValuePair> params = new ArrayList<>();
			params.add(new BasicNameValuePair("code", request.getParameter("code")));
			params.add(new BasicNameValuePair("client_id", googleApiClientId));
			params.add(new BasicNameValuePair("client_secret", secret));
			params.add(new BasicNameValuePair("redirect_uri", baseUrl + LOCAL_GOOGLEAUTH_PATH));
			params.add(new BasicNameValuePair("grant_type", "authorization_code"));
			params.add(new BasicNameValuePair("scope", ""));

			httppost.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));

			final HttpResponse response = httpclient.execute(httppost);

			final BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
			final StringBuilder builder = new StringBuilder();
			for (String line = null; (line = reader.readLine()) != null;) {
				builder.append(line).append("\n");
			}

			JsonNode json = mapper.readTree(builder.toString());

			return json.has("access_token") ? json.get("access_token").textValue() : null;
		} finally {
			httpclient.close();
		}
	}

	public String getAuthenticationUrl() {
		// google.auth.url=https://accounts.google.com/o/oauth2/auth?redirect_uri=http://localhost:8080/g/auth&response_type=code&client_id=12345-hfp8qjfeqaefpitbc707uluuh8vq65k7.apps.googleusercontent.com&scope=https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fplus.login+https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fplus.me+https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fuserinfo.email+https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fuserinfo.profile&approval_prompt=auto&access_type=online&include_granted_scopes=true
		final List<NameValuePair> parameters = new ArrayList<NameValuePair>();
		parameters.add(new BasicNameValuePair("redirect_uri", baseUrl + LOCAL_GOOGLEAUTH_PATH));
		parameters.add(new BasicNameValuePair("response_type", "code"));
		parameters.add(new BasicNameValuePair("client_id", googleApiClientId));
		parameters.add(new BasicNameValuePair("approval_prompt", "auto"));
		parameters.add(new BasicNameValuePair("access_type", "online"));
		parameters.add(new BasicNameValuePair("include_granted_scopes", "true"));
		// Google+
		// "https://www.googleapis.com/auth/plus.login https://www.googleapis.com/auth/plus.me https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile"));
		// Only basic:
		parameters.add(new BasicNameValuePair("scope", "https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/userinfo.email"));
		final String query = URLEncodedUtils.format(parameters, "UTF-8");
		return "https://accounts.google.com/o/oauth2/auth?" + query;
	}

	public Authentication googleAuthentication(Person userInfo) {
		try {
			final UserDetails userDetails = userDetailsService.loadUserByUsername(userInfo.getAccountEmail());

			if (!(userDetails.isEnabled() && userDetails.isAccountNonExpired() && userDetails.isAccountNonLocked() && userDetails.isCredentialsNonExpired())) {
				LOG.warn("Google login canceled: Account currently not available: " + userInfo.getAccountEmail());
				return null;
			}

			final Authentication authentication = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
			SecurityContextHolder.getContext().setAuthentication(authentication);

			return authentication;
		} catch (final UsernameNotFoundException e) {
			LOG.warn("Authentication with Google+ failed: No such user " + userInfo.getAccountEmail());
			return null;
		}
	}
}
